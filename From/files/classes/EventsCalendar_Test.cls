@isTest 
public class EventsCalendar_Test 
{
	 static testMethod void WithoutRecurring() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id, Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addMinutes(600), Start_Date__c=DateTime.now().addMinutes(500), End_Date__c=DateTime.now().addMinutes(600), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
		    
			

			Test.setCurrentPage(pageRef);

			EventsCalendar testAccPlan = new EventsCalendar();
			testAccPlan.pageLoad();
     		 testAccPlan.selectedEventCategory = '-';
         testAccPlan.eventCategoryDropdownChangeHandler();
         EventListController ev = new EventListController();
         ev.getMyRegistrations();
		Test.StopTest();
	 }
	 
	  static testMethod void WithRecurring() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
           Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Type__c='Alumni', Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addMinutes(600), End_Date__c=DateTime.now().addMinutes(600), Start_Date__c=DateTime.now().addMinutes(500), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
		
		Test.StartTest(); 
     
           	
			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
            
			pageRef.getParameters().put('searchString', String.valueOf(testEvent.Name));
        
          
			Test.setCurrentPage(pageRef);
        	EventsCalendar testAccPlan = new EventsCalendar();
         
            testAccPlan.pageLoad();
            testAccPlan.searchEvent();
         testAccPlan.selectedEventCategory = 'Type';
         testAccPlan.selectedEventName     = testEvent.Type__c;
         testAccPlan.eventCategoryDropdownChangeHandler();
         testAccPlan.eventNameChangeHandler();
		Test.StopTest();
	 }
	 
	  static testMethod void WithRecurringWeekly() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
           Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Weekly__c=true,Monday__c = true,Type__c='Alumni', Category__c='New Student', Tuesday__c=true,Wednesday__c=true,Thursday__c=true,Friday__c=true,Saturday__c=true,Sunday__c=true,Event_Location__c='On-Campus', Registration_Fee__c=100,
        Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addMonths(12).addMinutes(600),Start_Date__c=DateTime.now().addMinutes(500),
		End_Date__c=DateTime.now().addMinutes(600), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
		
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
            pageRef.getParameters().put('evntID', String.valueOf(testEvent.Id));
			pageRef.getParameters().put('searchString', 'testEvent');
			

			Test.setCurrentPage(pageRef);

			EventsCalendar testAccPlan = new EventsCalendar();
			testAccPlan.pageLoad();
           testAccPlan.selectedEventCategory = 'Category';
          testAccPlan.selectedEventName     = testEvent.Category__c;
         testAccPlan.eventCategoryDropdownChangeHandler();
          testAccPlan.eventNameChangeHandler();
		Test.StopTest();
	 }

	 
	  static testMethod void WithRecurringMonthly() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
           Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Monthly__c=true,Day_of__c='2nd',Day__c='Monday', Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addMonths(12).addMinutes(600), Start_Date__c=DateTime.now().addMinutes(500), End_Date__c=DateTime.now().addMonths(12).addMinutes(600), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
		
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
           
			pageRef.getParameters().put('searchString', 'testEvent');
			Test.setCurrentPage(pageRef);

			EventsCalendar testAccPlan = new EventsCalendar();
			testAccPlan.pageLoad();	
           testAccPlan.selectedEventCategory = 'Program';
         testAccPlan.selectedEventName     = 'testProg';
         testAccPlan.eventCategoryDropdownChangeHandler();
          testAccPlan.eventNameChangeHandler();
		Test.StopTest();
	 }
	 
	  static testMethod void WithRecurringYearly() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
           Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Yearly__c=true,Day_of1__c='2nd',Every_Year__c='January',Day1__c='Sunday', Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100,
        Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addYears(12).addMinutes(600),Start_Date__c=DateTime.now().addMinutes(500),
		End_Date__c=DateTime.now().addMinutes(600), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
		
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
           
			pageRef.getParameters().put('searchString', 'testEvent');
			Test.setCurrentPage(pageRef);

			EventsCalendar testAccPlan = new EventsCalendar();
			testAccPlan.pageLoad();	
          testAccPlan.selectedEventCategory = 'Department';
        testAccPlan.selectedEventName     = 'TestDepart';
         testAccPlan.eventCategoryDropdownChangeHandler();
          testAccPlan.eventNameChangeHandler();
		Test.StopTest();
	 }
	
    static testMethod void WithRoomID() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id, Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addMinutes(600), Start_Date__c=DateTime.now().addMinutes(500), End_Date__c=DateTime.now().addMinutes(600), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

		PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
		pageRef.getParameters().put('roomId', String.valueOf(testRoom.Id));
        pageRef.getParameters().put('evntID', String.valueOf(testEvent.Id));
	    pageRef.getParameters().put('selectedEventCategory', 'TYPE');
		pageRef.getParameters().put('selectedEventName', 'New Student');
		pageRef.getParameters().put('searchString', 'testEvent');

		Test.setCurrentPage(pageRef);

		EventsCalendar testAccPlan = new EventsCalendar();
		testAccPlan.pageLoad();

		Test.StopTest();
	 }
    
     static testMethod void WithoutEventId() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id, Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(500), Event_Setup_End_Time__c=DateTime.now().addMinutes(600), Start_Date__c=DateTime.now().addMinutes(500), End_Date__c=DateTime.now().addMinutes(600), Public__c = true);
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			

			Test.setCurrentPage(pageRef);

			EventsCalendar testAccPlan = new EventsCalendar();
			testAccPlan.pageLoad();
             testAccPlan.selectedEventCategory = 'School';
           testAccPlan.selectedEventName     = 'testSchool';
         testAccPlan.eventCategoryDropdownChangeHandler();
          testAccPlan.eventNameChangeHandler();		
		Test.StopTest();
	 }
	 
}