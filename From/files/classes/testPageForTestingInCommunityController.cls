public class testPageForTestingInCommunityController {
    public List<Event__c> eventsList {get;set;}
    
    public testPageForTestingInCommunityController() {
        Map<Id, Event__c> childEventsList = new Map<Id, Event__c>([SELECT Parent_Event__c FROM Event__c WHERE Parent_Event__c != NULL]);
        Set<Id> parentEventsID = new Set<Id>();
        eventsList = [SELECT Name, Accept_Discount_Code__c FROM Event__c WHERE Id NOT IN: parentEventsID AND Id NOT IN: childEventsList.keySet() AND Public__c = TRUE AND Start_Date__c >: System.today().addDays(-1) ORDER BY Name];
    }
    
    @AuraEnabled
    public static List<Event__c> get_EventsList()
    {
    	Map<Id, Event__c> childEventsList = new Map<Id, Event__c>([SELECT Parent_Event__c FROM Event__c WHERE Parent_Event__c != NULL]);
        Set<Id> parentEventsID = new Set<Id>();
        return [SELECT Name, Accept_Discount_Code__c FROM Event__c WHERE Id NOT IN: parentEventsID AND Id NOT IN: childEventsList.keySet() AND Public__c = TRUE AND Start_Date__c >: System.today().addDays(-1) ORDER BY Name];
    }

    @AuraEnabled
    public static String get_DiscountCode(Id eventId)
    {
        List<Discount_Code__c> discountRecord = [SELECT Id FROM Discount_Code__c WHERE Discount_Code__c =: eventId ORDER BY CREATEDDATE DESC NULLS LAST];
        if(!discountRecord.isEmpty())
            return discountRecord[0].Id;
        else
            return '';
    }
}