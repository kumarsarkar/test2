public class GetDateList {
   public list<DateTime> endDateList      = new list<DateTime>();
   public list<dateTime> startdateList    = new list<dateTime>();
   public list<string> dayList            = new list<string> {'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'};
   public list<string> IntervalList       = new list<string> {'1st','2nd','3rd','4th','Last'};
   public list<string> monthList          = new list<string>{'January','February','March','April','May','June','July','August','September','October','November', 'December'}; 
   public map<string,integer> dayMap      = new map<string,integer>();
   public map<string,integer> IntervalMap = new map<string,integer>();
   public map<string,integer> monthMap    = new map<string,integer>();
   public  DateTime startDT;
   public  DateTime endDT;
   public DateTime eventStartDate1;
   public DateTime eventEndDate1; 
   String dtFormat                        = 'EEE, d MMM yyyy HH:mm:ss z';
    public GetDateList(){

         integer D = 1;
         integer Ini = 0;
         integer m   = 1;
        
        for(string mnth : monthList){
            monthMap.put(mnth, m);
            m = m + 1;
        }
           for(string day : dayList){
                dayMap.put(day, D);
                D = D+1;
               system.debug('dayMap ' + dayMap);
            }
        for(string intr : IntervalList){
            IntervalMap.put(intr, Ini);
                Ini = Ini + 7;
        }
    }
    public void  generateCalendarData(List<Event__c> eventsList) {
         Datetime startOfMonth = Date.Today().toStartOfMonth();
        Datetime startOfNextMonth = startOfMonth.addMonths(1);
        try{
        for(Event__c e : eventsList)
        {
             system.debug('eventsList  22222@@@@@@@@@@@@' + eventsList);
            
              startDT = eventStartDate1;
              endDT  = eventEndDate1;
           
          system.debug('startDT &&& ' + startDT);
            Date startD = date.newinstance(startDT.year(), startDT.month(), startDT.day());
            Date endD = date.newinstance(endDT.year(), endDT.month(), endDT.day());
            
            Integer noOfDays = startD.daysBetween(endD);
          
                list<dateTime > datesBetweenStartEnd = new list<dateTime>();
              
                 for(Integer i=0; i<noOfDays; i++){
                    DateTime start = startDT.addDays(i);
                    datesBetweenStartEnd.add(start);
                                     }
            
           if(!e.Recurring__c && !e.Weekly__c && !e.Monthly__c && !e.Yearly__c)
            {
              
                          startdateList.add(startDT);
                          enddateList.add(endDT);
                      
            }
            else if(e.Recurring__c && !e.Weekly__c && !e.Monthly__c && !e.Yearly__c )
            {
              
               system.debug('recurring');
                for(Integer i=0; i<noOfDays; i++){
                    dateTime startdate1 = startDT.addDays(i);
                    DateTime endDate  = getEndDate(startdate1,endDT);
                          startdateList.add(startdate1);
                          enddateList.add(endDate);
                    system.debug('enddateList  ' + enddateList);
                  
                }
            } else if(e.Weekly__c && e.Recurring__c ){
               
             dateTime startDate  = startDT;
              
                for(dateTime dt1 : datesBetweenStartEnd){
                   String dayOfWeek = dt1.format('E');
                    

                    if(dayOfWeek == 'Sun' && e.Sunday__c == true){
                
                          datetime stDate ;
                         
                          stDate   = dt1; 
                          DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                     system.debug('startdateList@@@@@@@@111  ' + startdateList);
                }
                if(dayOfWeek == 'Mon' && e.Monday__c == true){
                    
                          datetime stDate ;
                          stDate   = dt1; 
                          DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                      
                }
                if(dayOfWeek == 'Tue' && e.Tuesday__c == true){
                   
                          datetime stDate ;
                           stDate   = dt1; 
                           DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                        system.debug('startdateList@@@@@@@@111  ' + startdateList);

                }
                
               if(dayOfWeek == 'Wed' && e.Wednesday__c == true){
                   
                          datetime stDate ;
                          stDate   = dt1; 
                          DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                      

                }
                
               if(dayOfWeek == 'Thu' && e.Thursday__c == true){
                   
                          datetime stDate ;
                          stDate   = dt1; 
                          DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                      
                   
                }
               if(dayOfWeek == 'Fri' && e.Friday__c == true){
                   
                          datetime stDate ;
                          stDate   = dt1; 
                          DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                       

                }
               if(dayOfWeek == 'Sat' && e.Saturday__c == true){
                   
                          datetime stDate ;
                          stDate   = dt1; 
                          DateTime endDate  = getEndDate(dt1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                     

                }
                }
            } else if(e.Monthly__c && e.Recurring__c){
                system.debug('Monthly@@@@@@@');
                Integer monthDiff = startD.monthsBetween(endD);
               if (endD.day() > startD.day()) monthDiff++;
                for(integer i = 0; i<=monthDiff ; i++){
                    Date startDate1 = startD.addMonths(i);
                    
                   
                   Date EventDay = Date.newinstance(startDate1.year(), startDate1.month(), startDate1.day()).toStartOfMonth().toStartOfWeek().addDays(dayMap.get(e.Day__c));
                   if(EventDay < Date.newinstance(startDate1.year(), startDate1.month(), startDate1.day()).toStartOfmonth()) EventDay = EventDay.addDays(7); 
                    
                     date checkdate = EventDay.addDays(IntervalMap.get(e.Day_of__c));
                        string intrvl ;
                        Integer numberOfDays = Date.daysInMonth(EventDay.year(), EventDay.month());
                        Date lastDayOfMonth = Date.newInstance(EventDay.year(), EventDay.month(), numberOfDays);
                        if( checkdate>lastDayOfMonth){
                            intrvl = '4th';
                        }else{
                           intrvl  =  e.Day_of__c;
                        }
                    
                   Date FinalEventDay = EventDay.addDays(IntervalMap.get(intrvl));
                    dateTime startdateTime = FinalEventDay;
               
                    if(FinalEventDay >=startD && FinalEventDay <=endD){
                    
                     dateTime startMDate = getEndDate(startdateTime.addDays(1).addHours(-6),startDT);
                       datetime finalStartDate = startMDate;
                        dateTime finalEndDate  = getEndDate(startMDate,endDT);
                         
                          datetime stDate ;
                          dateTime endDate ;
                          stDate   = finalStartDate; 
                          endDate  = finalEndDate;
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                     system.debug('startdateList%%%&&  ' + startdateList);
                    }
                    
                        
                }        
            }else if(e.Yearly__c && e.Recurring__c){
               integer yearDiff = endD.year() - startD.year();
               
                for(integer k = 0;k<=yearDiff;k++){
                       integer monthNo = startD.month();
                       integer monthNo2 = monthMap.get(e.Every_Year__c);
                    Date startDate2 = startD.addYears(k).addMonths(monthNo2-monthNo);
                 
                   
                   Date EventDay1 = Date.newinstance(startDate2.year(), startDate2.month(), startDate2.day()).toStartOfMonth().toStartOfWeek().addDays(dayMap.get(e.Day1__c));
                   if(EventDay1 < Date.newinstance(startDate2.year(), startDate2.month(), startDate2.day()).toStartOfmonth()) EventDay1 = EventDay1.addDays(7); 
                   
                     date checkdate = EventDay1.addDays(IntervalMap.get(e.Day_of__c));
                        string intrvl ;
                        Integer numberOfDays = Date.daysInMonth(EventDay1.year(), EventDay1.month());
                        Date lastDayOfMonth = Date.newInstance(EventDay1.year(), EventDay1.month(), numberOfDays);
                        if( checkdate>lastDayOfMonth){
                            intrvl = '4th';
                        }else{
                           intrvl  =  e.Day_of__c;
                        }
                    Date FinalEventDay1 = EventDay1.addDays(IntervalMap.get(intrvl));
                
                    if(FinalEventDay1 >= startD && FinalEventDay1 <= endD){
                       
                          datetime stDate ;
                          dateTime endDate ;
                          stDate   = FinalEventDay1; 
                          endDate  =  getEndDate(FinalEventDay1,endDT);
                          startdateList.add(stDate);
                          enddateList.add(endDate);
                      
                    }
                }
            }
            
            
        }
            }
            catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
        }
        
    }
    
    public static datetime getEndDate(dateTime satrtD , dateTime endD){
        try{
            integer Eh = Integer.valueof(endD.format('HH'));
            integer Em = Integer.valueof(endD.format('mm'));
            integer Sh = Integer.valueof(satrtD.format('hh'));
            integer Sm = Integer.valueof(satrtD.format('mm'));
                         integer hh  = Eh-Sh;
                         integer mm  = Em-Sm;
           dateTime endTime = satrtD.addHours(hh).addMinutes(mm) ;
        return endTime;
              }
            catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
                   return null;
        }
    }
    
   public void pageLoad(string evId,datetime eventStartDate, dateTime eventEndDate)
    {
     system.debug('eventStartDate@@###$$$##@@@  ' + eventStartDate);
        try{
            eventStartDate1 = eventStartDate;
            eventEndDate1 = eventEndDate;
     Date startOfMonth = Date.Today().toStartOfMonth();
        Date startOfNextMonth = startOfMonth.addMonths(1);
            List<Event__c> eventsList = [SELECT Id,
                                         Event_Listing_Description__c,
                                         Every_Year__c,
                                         Day_of1__c,
                                         Day1__c,
                                         Start_Date__c,
                                         End_Date__c,
                                         Every_Month__c,
                                         Recurring__c,
                                         Weekly__c,
                                         Monthly__c,
                                         Day_of__c,
                                         Day__c,
                                         Yearly__c,
                                         Monday__c,
                                         Tuesday__c,
                                         Wednesday__c,
                                         Thursday__c,
                                         Friday__c,
                                         Saturday__c,
                                         Sunday__c,
                                         Event_Setup_Start_Time__c,
                                         Event_Setup_End_Time__c,
                                         Name FROM Event__c
                                         WHERE Id =: evId
                                         
                                         Limit 1000];
        
                generateCalendarData(eventsList);
         
         }
            catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
                  
        }
   
    }
    
}