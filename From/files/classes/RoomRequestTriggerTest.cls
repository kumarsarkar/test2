@isTest
public Class RoomRequestTriggerTest{
    static testMethod void TestTriggerMethod(){
        Event__c eventRecord = New Event__c(Name='Testing',Category__c='Alumni Relations',Event_Location__c='On-Campus');
        insert eventRecord;
        Building__c buildingRecord = New Building__c(Name='Testing');
        insert buildingRecord;
        Room__c roomRecord = New Room__c(Name='Testing',Building__c=buildingRecord.Id,Room_Number__c='123');
        insert roomRecord;
        Room_Request__c roomRequestRecord = New Room_Request__c(Room__c =roomRecord.Id,Event__c=eventRecord.Id,isPrimary__c=True,Status__c='Pending Approval');
        insert roomRequestRecord;
        roomRequestRecord.Status__c = 'Approved';
        update roomRequestRecord;
        
        
        
    }

}