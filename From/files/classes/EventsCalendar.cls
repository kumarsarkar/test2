public class EventsCalendar {
    
    public list<calEvent> events {get;set;}                // list displayed in the VFP.
    public list<DateTime> startdateList   = new list<DateTime>();
    public list<DateTime> enddateList     = new list<DateTime>();
    public set< string> schoolEventsList  = new set<string>();
    public set< string> deptEventsList    = new set<string>();
    public set< string> programEventsList = new set<string>();
    public set< string> categoryList      = new set<string>();
    public set< string> TypeList          = new set<string>();
    public List<SelectOption> eventCategoriesList {get;set;}    // dropdown 1 displaying the categories of events
    public List<SelectOption> subEventNamesList {get;set;}    // dropdown 2 displaying all the event names of the category selected
    
    public String selectedEventCategory {get;set;}            // selected value from dropdown 1
    public String selectedEventName {get;set;}            // selected value from dropdown 2
    public String searchString {get;set;}                    // search String holding the input field value
    public String roomId {get;set;}
    public String eventId {get;set;}
    public list<string> dayList = new list<string> {'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'};
    public list<string> IntervalList = new list<string> {'1st','2nd','3rd','4th','Last'};
    public list<string> monthList    = new list<string>{'January','February','March','April','May','June','July','August','September','October','November', 'December'}; 
    public map<string,integer> dayMap = new map<string,integer>();
    public map<string,integer> IntervalMap = new map<string,integer>();
    public map<string,integer> monthMap = new map<string,integer>();
    public  DateTime startDT;
    public  DateTime endDT;
    public string Defaultday{get;set;}
    
    // ADDED BY Kumar Sarkar, 12-06-12
    public String loggedinUserProfileName;
    
    // The calendar plugin is expecting dates is a certain format. We use this string to get it formated correctly
    String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
    
    // make the data ready for the page load
    public EventsCalendar()
    {
        system.debug('Constructor() executed.');
        
        // ADDED BY Kumar Sarkar, 12-06-12
        loggedinUserProfileName = [Select Id,Name from Profile where Id =: Userinfo.getProfileId()].Name;
        
        list<Event__c>             eList = [select id,Category__c,Type__c from Event__c];
        List< School_College__c>  scList = [SELECT Name FROM School_College__c];
        List< Department__c>     DepList = [SELECT Name FROM Department__c];
        List< Program__c>       ProgList = [SELECT Name FROM Program__c];
        
        for(Event__c c : eList){
            if(c.Category__c <> null ){
                categoryList.add(c.Category__c);
            }
            if(c.Type__c <> null ){
                TypeList.add(c.Type__c);
            }
        }
        for(School_College__c sc : scList){
            schoolEventsList.add(sc.Name);            
        } 
        for(Department__c d : DepList){
            
            deptEventsList.add(d.Name);            
        } 
        for(Program__c d : ProgList){
            programEventsList.add(d.Name);            
        } 
        
        eventCategoriesList = new List<SelectOption>();
        eventCategoriesList.add(new SelectOption('-','-'));
        eventCategoriesList.add(new SelectOption('School/College','School/College'));
        eventCategoriesList.add(new SelectOption('Department','Department'));
        eventCategoriesList.add(new SelectOption('Program','Program'));
        eventCategoriesList.add(new SelectOption('Category','Category'));
        eventCategoriesList.add(new SelectOption('Type','Type'));
        
        integer D = 1;
        integer Ini = 0;
        integer m   = 1;
        
        for(string mnth : monthList){
            monthMap.put(mnth, m);
            m = m + 1;
        }
        
        for(string day : dayList){
            dayMap.put(day, D);
            D = D+1;
            system.debug('dayMap ' + dayMap);
        }
        
        for(string intr : IntervalList){
            IntervalMap.put(intr, Ini);
            Ini = Ini + 7;
        }
    }
    
    // calendar event generator out of our event Custom object records
    public void  generateCalendarData(List<Event__c> eventsList) {
        system.debug('************events ######' + events);
        DateTime startOfMonth = Date.Today().toStartOfMonth();
        DateTime startOfNextMonth = startOfMonth.addMonths(1);
        try{ 
            for(Event__c e : eventsList)
            {
                if(e.Event_Setup_Start_Time__c == null && e.Event_Setup_End_Time__c == null){
                    startDT = e.Start_Date__c;
                    endDT  = e.End_Date__c;
                }else{
                    startDT = e.Event_Setup_Start_Time__c;
                    endDT = e.Event_Setup_End_Time__c; 
                }
                
                Date startD = date.newinstance(startDT.year(), startDT.month(), startDT.day());
                Date endD = date.newinstance(endDT.year(), endDT.month(), endDT.day());
                
                Integer noOfDays = startD.daysBetween(endD);
                system.debug('startDT ' + startDT);
                
                list<dateTime > datesBetweenStartEnd = new list<dateTime>();
                map<integer,DateTime> dateMap   =     new map<integer,DateTime>();
                list<dateTime > days = new list<dateTime>();
                for(Integer i=0; i<=noOfDays; i++){
                    DateTime start = startDT.addDays(i);
                    datesBetweenStartEnd.add(start);
                }
                
                if(!e.Recurring__c && !e.Weekly__c && !e.Monthly__c && !e.Yearly__c)
                {
                    calEvent myEvent = new calEvent();
                    myEvent.title = e.Name;
                    myEvent.allDay = false;
                    myEvent.startString = startDT.format(dtFormat);
                    myEvent.endString   = endDT.format(dtFormat) ;
                    //ADDED BY Kumar Sarkar, 12-06-12 [START]
                    if(loggedinUserProfileName.toUpperCase().contains('ADMIN'))
                        myEvent.url = e.Id;
                    else if(loggedinUserProfileName.toUpperCase().contains('STUDENT'))
                        myEvent.url = 's/event-registration-community?recordId=' + e.Id;
                    //ADDED BY Kumar Sarkar, 12-06-12 [END]
                    myEvent.className = 'event-personal';
                    events.add(myEvent);
                }
                else if(e.Recurring__c && !e.Weekly__c && !e.Monthly__c && !e.Yearly__c)
                {
                    for(Integer i=0; i<=noOfDays; i++){
                        dateTime startdate1 = startDT.addDays(i);
                        
                        calEvent myEvent = new calEvent();
                        myEvent.title = e.Name;
                        myEvent.allDay = false;
                        myEvent.startString = startdate1.format(dtFormat);
                        myEvent.endString   = getEndDate(startdate1,endDT).format(dtFormat);
                        //ADDED BY Kumar Sarkar, 12-06-12 [START]
                        if(loggedinUserProfileName.toUpperCase().contains('ADMIN'))
                            myEvent.url = e.Id;
                        else if(loggedinUserProfileName.toUpperCase().contains('STUDENT'))
                            myEvent.url = 's/event-registration-community?recordId=' + e.Id;
                        //ADDED BY Kumar Sarkar, 12-06-12 [END]
                        myEvent.className = 'event-personal';
                        events.add(myEvent);
                    }
                }
                else if(e.Weekly__c && e.Recurring__c){
                    
                    dateTime startDate  = startDT;
                    DateTime endDate  = endDT;
                    
                    for(dateTime dt : datesBetweenStartEnd){
                        
                        String dayOfWeek = dt.format('E');
                        calEvent myEvent = new calEvent();
                        myEvent.title = e.Name;
                        myEvent.allDay = false;
                        //ADDED BY Kumar Sarkar, 12-06-12 [START]
                        if(loggedinUserProfileName.toUpperCase().contains('ADMIN'))
                            myEvent.url = e.Id;
                        else if(loggedinUserProfileName.toUpperCase().contains('STUDENT'))
                            myEvent.url = 's/event-registration-community?recordId=' + e.Id;
                        //ADDED BY Kumar Sarkar, 12-06-12 [END]
                        myEvent.className = 'event-personal';
                        
                        if(dt.format('E') == 'Mon' && e.Monday__c == true){
                            
                            system.debug('dt@@@@@dt ' + dt);
                            
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            system.debug('myEvent.startString  ' + myEvent.startString);
                            system.debug('getEndDate(dt,endDT) ' + getEndDate(dt,endDT));
                            events.add(myEvent);
                            
                        }
                        if(dayOfWeek == 'Tue' && e.Tuesday__c == true){
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            events.add(myEvent);
                            system.debug('myEvent.endString   ' + myEvent.endString );
                            system.debug('myEvent.startString  ' + myEvent.startString );
                        }
                        
                        if(dayOfWeek == 'Wed' && e.Wednesday__c == true){
                            
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            events.add(myEvent);
                            
                        }
                        
                        if(dayOfWeek == 'Thu' && e.Thursday__c == true){
                            
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            events.add(myEvent);
                            
                        }
                        if(dayOfWeek == 'Fri' && e.Friday__c == true){
                            
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            
                            events.add(myEvent);
                        }
                        if(dayOfWeek == 'Sat' && e.Saturday__c == true){
                            
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            
                            events.add(myEvent);
                        }
                        if(dayOfWeek == 'Sun' && e.Sunday__c == true){
                            
                            myEvent.startString = dt.format(dtFormat);
                            myEvent.endString   = getEndDate(dt,endDT).format(dtFormat);
                            
                            events.add(myEvent);
                        }
                    }
                } else if(e.Monthly__c && e.Recurring__c){
                    dateTime dttttt = startD;
                    system.debug('startD@@@2 ' 
                                 + dttttt.format(dtFormat));
                    Integer monthDiff = startD.monthsBetween(endD);
                    if (endD.day() > startD.day()) monthDiff++;
                    for(integer i = 0; i<=monthDiff ; i++){
                        
                        Date startDate1 = startD.addMonths(i);
                        if(!(e.Day_of__c == '')){
                            
                            Date EventDay = Date.newinstance(startDate1.year(), startDate1.month(), startDate1.day()).toStartOfMonth().toStartOfWeek().addDays(dayMap.get(e.Day__c));
                            if(EventDay < Date.newinstance(startDate1.year(), startDate1.month(), startDate1.day()).toStartOfmonth()) EventDay = EventDay.addDays(7);
                            date checkdate = EventDay.addDays(IntervalMap.get(e.Day_of__c));
                            string intrvl ;
                            Integer numberOfDays = Date.daysInMonth(EventDay.year(), EventDay.month());
                            Date lastDayOfMonth = Date.newInstance(EventDay.year(), EventDay.month(), numberOfDays);
                            if( checkdate>lastDayOfMonth){
                                intrvl = '4th';
                            }else{
                                intrvl  =  e.Day_of__c;
                            }
                            Date FinalEventDay = EventDay.addDays(IntervalMap.get(intrvl));
                            dateTime startdateTime = FinalEventDay;
                            
                            system.debug('startdateTime  ##@@@@@@ ' + startdateTime.format(dtFormat));
                            if(FinalEventDay >=startD && FinalEventDay <=endD){
                                
                                dateTime startMDate = getEndDate(startdateTime.addDays(1).addHours(-12),startDT);
                                calEvent myEvent = new calEvent();
                                myEvent.title       = e.Name;
                                myEvent.allDay      = false;
                                myEvent.startString = startMDate.format(dtFormat);
                                myEvent.endString   = getEndDate(startMDate,endDT).format(dtFormat);
                                //ADDED BY Kumar Sarkar, 12-06-12 [START]
                                if(loggedinUserProfileName.toUpperCase().contains('ADMIN'))
                                    myEvent.url = e.Id;
                                else if(loggedinUserProfileName.toUpperCase().contains('STUDENT'))
                                    myEvent.url = 's/event-registration-community?recordId=' + e.Id;
                                //ADDED BY Kumar Sarkar, 12-06-12 [END]
                                myEvent.className   = 'event-personal';
                                
                                events.add(myEvent);
                                system.debug('myEvent.startString@@## ' + myEvent.startString);
                                system.debug(' myEvent.endString @@##' +  myEvent.endString);
                            }
                        } 
                    }
                    
                }else if(e.Yearly__c && e.Recurring__c){
                    integer yearDiff = endDT.year() - startDT.year();
                    
                    for(integer k = 0;k<=yearDiff;k++){
                        integer monthNo = startD.month();
                        integer monthNo2 = monthMap.get(e.Every_Year__c);
                        Date startDate2 = startD.addYears(k).addMonths(monthNo2-monthNo);
                        
                        
                        Date EventDay1 = Date.newinstance(startDate2.year(), startDate2.month(), startDate2.day()).toStartOfMonth().toStartOfWeek().addDays(dayMap.get(e.Day1__c));
                        if(EventDay1 < Date.newinstance(startDate2.year(), startDate2.month(), startDate2.day()).toStartOfmonth()) EventDay1 = EventDay1.addDays(7);
                        
                        date checkdate = EventDay1.addDays(IntervalMap.get(e.Day_of__c));
                        string intrvl ;
                        Integer numberOfDays = Date.daysInMonth(EventDay1.year(), EventDay1.month());
                        Date lastDayOfMonth = Date.newInstance(EventDay1.year(), EventDay1.month(), numberOfDays);
                        if( checkdate>lastDayOfMonth){
                            intrvl = '4th';
                        }else{
                            intrvl  =  e.Day_of__c;
                        }
                        
                        DateTime FinalEventDay1 = EventDay1.addDays(IntervalMap.get(intrvl));
                        system.debug('FinalEventDay1@@##$$ ' + FinalEventDay1.format(dtFormat));
                        if(FinalEventDay1 >= startDT && FinalEventDay1 <= endDT){
                            dateTime startMDate2 = getEndDate(FinalEventDay1.addDays(1).addHours(-12),startDT);
                            calEvent myEvent = new calEvent();
                            myEvent.title       = e.Name;
                            myEvent.allDay      = false;
                            myEvent.startString = startMDate2.format(dtFormat);
                            myEvent.endString   =getEndDate(startMDate2,endDT).format(dtFormat);
                            //ADDED BY Kumar Sarkar, 12-06-12 [START]
                            if(loggedinUserProfileName.toUpperCase().contains('ADMIN'))
                                myEvent.url = e.Id;
                            else if(loggedinUserProfileName.toUpperCase().contains('STUDENT'))
                                myEvent.url = 's/event-registration-community?recordId=' + e.Id;
                            //ADDED BY Kumar Sarkar, 12-06-12 [END]
                            myEvent.className   = 'event-personal';
                            
                            events.add(myEvent);
                            
                        }
                    }
                }
            }
        } catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
            
        }
        
    }
    
    // calendar event generator out of our room requests Custom object records
    public void generateCalendarData(List<Room_Request__c> roomsList) {
        
        Date startOfMonth = Date.Today().toStartOfMonth();
        Date startOfNextMonth = startOfMonth.addMonths(1);
        try{
            for(Room_Request__c roomsListVar : roomsList)
            {
                DateTime startDT = roomsListVar.Start_DateTime__c;
                DateTime endDT = roomsListVar.End_DateTime__c;
                
                // Date edate = date.newinstance(eventStartdateRoom.year(), eventStartdateRoom.month(), eventStartdateRoom.day());
                Integer offset = UserInfo.getTimezone().getOffset(startDT);
                startDt = startDt.addSeconds(-1*offset/1000);
                offset = UserInfo.getTimezone().getOffset(endDt);
                endDt = endDt.addSeconds(-1*offset/1000);
                
                calEvent myRoom = new calEvent();
                myRoom.title = roomsListVar.Event__r.Name;
                myRoom.allDay = false;
                myRoom.startString = startDT.addHours(-6).format(dtFormat);
                myRoom.endString = endDT.addHours(-6).format(dtFormat);
                
                if(roomsListVar.Event__r.Student_Created_Event__c == true){
                    myRoom.url = '/develop-l-maryville.cs19.force.com/pmtx/apex/EventRegistration?Id=' + roomsListVar.Event__c + '&connectionId=' + Label.Payment_Connection_ID;
                }else{
                    myRoom.url  = roomsListVar.id;
                }
                myRoom.className = 'room-personal';
                events.add(myRoom);
            }
            
        } catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
            
        }
    }
    
    // Executed when this page level action gets called on page load
    public void pageLoad()
    {
        roomId = '';
        eventId = '';
        string EID = '';

        if(!String.isBlank(ApexPages.currentPage().getParameters().get('roomId'))){
            roomId = ApexPages.currentPage().getParameters().get('roomId');
            EID = ApexPages.currentPage().getParameters().get('evntID');
            system.debug('EID = ' + EID);
            
            Event__c eventsss = [select id,Start_Date__c,Event_Setup_Start_Time__c, Event_Setup_End_Time__c from Event__c where id =: EID AND Public__c = true];
            dateTime evDate;

            if(eventsss.Event_Setup_Start_Time__c == null && eventsss.Event_Setup_End_Time__c == null ){
                evDate = date.newinstance(eventsss.Start_Date__c.year(), eventsss.Start_Date__c.month(), eventsss.Start_Date__c.day());
                evDate = evDate.addDays(+1);  
            }else{
                evDate =date.newinstance(eventsss.Event_Setup_Start_Time__c.year(), eventsss.Event_Setup_Start_Time__c.month(), eventsss.Event_Setup_Start_Time__c.day());
                evDate = evDate.addDays(+1);
            }

            Defaultday = evDate.format();
        }
        else if(!String.isBlank(ApexPages.currentPage().getParameters().get('eventId'))){
            eventId = ApexPages.currentPage().getParameters().get('eventId');
            Defaultday = system.today().format();       
        } else{
            Defaultday = system.today().format();    
        }
        events = new list<calEvent>();
        
        Date startOfMonth = Date.Today().toStartOfMonth();
        Date startOfNextMonth = startOfMonth.addMonths(1);
        try{
            if(!String.isBlank(eventId))
            {
                List<Event__c> eventsList = [SELECT Id,
                                             Event_Listing_Description__c,
                                             Every_Year__c,
                                             Day_of1__c,
                                             Day1__c,
                                             Every_Month__c,
                                             Recurring__c,
                                             Weekly__c,
                                             Monthly__c,
                                             Day_of__c,
                                             Day__c,
                                             Yearly__c,
                                             Monday__c,
                                             Tuesday__c,
                                             Wednesday__c,
                                             Thursday__c,
                                             Friday__c,
                                             Saturday__c,
                                             Sunday__c,
                                             Start_Date__c,
                                             End_Date__c,
                                             Event_Setup_Start_Time__c,
                                             Event_Setup_End_Time__c,
                                             Name FROM Event__c
                                             WHERE Id =: eventId
                                                AND Public__c = true
                                             Limit 1000];
                generateCalendarData(eventsList);
            }
            else if(!String.isBlank(roomId))
            {
                List<Room_Request__c> roomsList = [SELECT Id,
                                                   Room__r.Name,
                                                   Event__c,
                                                   Event__r.Name,
                                                   Event__r.Start_Date__c,
                                                   Event__r.Public__c,
                                                   Event__r.Student_Created_Event__c ,
                                                   Room__c,
                                                   Start_DateTime__c,
                                                   End_DateTime__c FROM Room_Request__c
                                                   WHERE Room__c =: roomId
                                                   // COMMENTED BY Kumar Sarkar, 12-07-17 : AND Status__c IN ('Pending Approval', 'Approved', 'Waitlist')
                                                   AND Status__c IN ('Approved')
                                                   AND ((Start_DateTime__c >= :startOfMonth AND Start_DateTime__c < :startOfNextMonth)
                                                        OR (End_DateTime__c >= :startOfMonth AND End_DateTime__c < :startOfNextMonth)
                                                        OR (Start_DateTime__c < :startOfMonth AND End_DateTime__c > :startOfNextMonth))
                                                   Limit 1000];
                
                
                generateCalendarData(roomsList);
                
                system.debug('roomsList = ' + roomsList);
            }
            else
            {
                List<Event__c> eventsList = [SELECT Id,
                                             Event_Listing_Description__c,
                                             Every_Year__c,
                                             Day_of1__c,
                                             Day1__c,
                                             Every_Month__c,
                                             Recurring__c,
                                             Weekly__c,
                                             Monthly__c,
                                             Day_of__c,
                                             Day__c,
                                             Yearly__c,
                                             Monday__c,
                                             Tuesday__c,
                                             Wednesday__c,
                                             Thursday__c,
                                             Friday__c,
                                             Saturday__c,
                                             Sunday__c,
                                             Start_Date__c,
                                             End_Date__c,
                                             Event_Setup_Start_Time__c,
                                             Event_Setup_End_Time__c,
                                             Name FROM Event__c
                                             WHERE Public__c = true
                                             Limit 1000];
                generateCalendarData(eventsList);
            }
        } catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
        }
    }
    
    // Executed when filter dropdown (1) on event types is changed
    public void eventCategoryDropdownChangeHandler()
    {
        system.debug('eventCategoryDropdownChangeHandler() executed.');
        system.debug('selectedEventCategory ' + selectedEventCategory);
        subEventNamesList = new list<SelectOption>();
        subEventNamesList.add(new SelectOption('-','-'));
        selectedEventName = '';
        searchString = '';
        
        if(selectedEventCategory.toUpperCase().contains('SCHOOL'))
        {
            system.debug('schoolEventsList@@@@@2 ' + schoolEventsList.size());
            if(schoolEventsList<>null)
                for(string schoolEventsRec : schoolEventsList)
                subEventNamesList.add(new SelectOption(schoolEventsRec, schoolEventsRec));
        }
        
        else if(selectedEventCategory.toUpperCase().contains('DEPARTMENT'))
        {
            if(deptEventsList<>null)
                for(string deptEventsRec : deptEventsList)
                subEventNamesList.add(new SelectOption(deptEventsRec, deptEventsRec));
        }
        else if(selectedEventCategory.toUpperCase().contains('PROGRAM'))
        {
            if(programEventsList<>null)
                for(string programEventsRec : programEventsList)
                subEventNamesList.add(new SelectOption(programEventsRec, programEventsRec));
        }  else if(selectedEventCategory.toUpperCase().contains('CATEGORY'))
        {
            if(categoryList<>null)
                for(string programEventsRec : categoryList)
                subEventNamesList.add(new SelectOption(programEventsRec, programEventsRec));
        } else if(selectedEventCategory.toUpperCase().contains('TYPE'))
        {
            if(TypeList<>null)
                for(string programEventsRec : TypeList)
                subEventNamesList.add(new SelectOption(programEventsRec, programEventsRec));
        }
        else
        {
            pageLoad();
            subEventNamesList = null;
        }
    }
    
    //add Time into date 
    public static datetime getEndDate(dateTime satrtD , dateTime endD){
        try{
            integer Eh = Integer.valueof(endD.format('HH'));
            integer Em = Integer.valueof(endD.format('mm'));
            integer Sh = Integer.valueof(satrtD.format('hh'));
            integer Sm = Integer.valueof(satrtD.format('mm'));
            integer hh  = Eh-Sh;
            integer mm  = Em-Sm;
            dateTime endTime = satrtD.addHours(hh).addMinutes(mm) ;
            return endTime;
        }
        catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
            return null;
        }
    }
    
    // Executed when second dependent dropdown, event name, is changed
    public void eventNameChangeHandler()
    {
        searchString = '';
        String query = 'SELECT Id, Event_Listing_Description__c, Recurring__c,Monthly__c,Day_of__c,Day__c,Yearly__c, ';
        query += 'Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c,Every_Year__c,Day_of1__c,Every_Month__c,Weekly__c, ';
        query += 'Saturday__c, Sunday__c,Start_Date__c,End_Date__c, Event_Start_Date__c, Start_Time__c,Category__c,Event_Setup_Start_Time__c,Event_Setup_End_Time__c, ';
        query += 'Event_End_Date__c, End_Time__c, Name FROM Event__c WHERE Public__c = TRUE ';
        
        if(selectedEventCategory.toUpperCase().contains('SCHOOL'))
            query += ' AND School_College__c != NULL AND School_College__r.Name = \'' + selectedEventName + '\'';
        else if(selectedEventCategory.toUpperCase().contains('DEPARTMENT'))
            query += ' AND Department__c != NULL AND Department__r.Name = \'' + selectedEventName + '\'';
        else if(selectedEventCategory.toUpperCase().contains('PROGRAM'))
            query += ' AND Program__c != NULL AND Program__r.Name = \'' + selectedEventName + '\'';
        else if(selectedEventCategory.toUpperCase().contains('CATEGORY'))
            query += ' AND Category__c != NULL AND  Category__c = \'' + selectedEventName + '\'';
        else if(selectedEventCategory.toUpperCase().contains('TYPE'))
            query += ' AND Type__c != NULL AND  Type__c = \'' + selectedEventName + '\'';
        else
        {
            pageLoad();
            subEventNamesList = null;
        }
        
        query += ' LIMIT 1000';
        events = new list<calEvent>();
        
        Date startOfMonth = Date.Today().toStartOfMonth();
        Date startOfNextMonth = startOfMonth.addMonths(1);
        
        List<Event__c> eventsList = Database.query(query);
        
        generateCalendarData(eventsList);
        system.debug('eventsList ' + eventsList);
    }
    
    // Executed when the search box/engine is used
    public void searchEvent()
    {
        selectedEventName = '';
        
        Date startOfMonth = Date.Today().toStartOfMonth();
        Date startOfNextMonth = startOfMonth.addMonths(1);
        
        String query = 'SELECT Id, Event_Listing_Description__c, Recurring__c,Start_Date__c,End_Date__c, ';
        query += 'Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c,Weekly__c,Monthly__c,Every_Month__c,Day_of__c,Day__c,Yearly__c,Every_Year__c,Day1__c,Day_of1__c, ';
        query += 'Saturday__c, Sunday__c, Event_Start_Date__c, Start_Time__c,Event_Setup_Start_Time__c,Event_Setup_End_Time__c, ';
        query += 'Event_End_Date__c, End_Time__c, Name FROM Event__c WHERE Public__c = true ';
        
        query += ' AND Name LIKE \'%' + searchString + '%\'';
        
        query += ' LIMIT 1000';
        
        events = new list<calEvent>();
        
        List<Event__c> eventsList = Database.query(query);
        system.debug('eventsList');
        generateCalendarData(eventsList);
    }
    
    //Class to hold calendar event data
    public class calEvent
    {
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;private set;}
        public String endString {get;private set;}
        public String url {get;set;}
        public String className {get;set;}
    }
}