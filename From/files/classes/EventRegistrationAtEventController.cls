public class EventRegistrationAtEventController{
    
    public  string firstName {get;set;}
    public string lastName {get;set;}
    public string mailId {get;set;}
    public string eventName {get;set;}
    List<Event__c> lEvents = New List<Event__c>();
    List<Contact> lContact = New List<Contact>();
    List<Event_Registration__c> lRegistrations = New List<Event_Registration__c>();
    List<Room_Request__c> roomRequestRecord = New List<Room_Request__c>();  
    public String emailRegex;
    public String primaryBuildingName;
    public String primaryRoomNumber;
    public EventRegistrationAtEventController(){}
    
    public EventRegistrationAtEventController(String firstName,String lastName,String mailId,String eventName) {
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.mailId = mailId;
        this.eventName = eventName;
        system.debug('first name'); 
    }
    
    public PageReference validateInputData(){
        
        If(string.isBlank(firstName) || string.isBlank(lastName) || string.isBlank(mailId) || string.isBlank(eventName)){
            ApexPages.Message msg = new ApexPages.message(ApexPages.severity.ERROR, 'Please fill up all the fields');
            ApexPages.addMessage(msg);
            return null;
        }
        
        emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(mailId);
        
        lContact = [SELECT Id, FirstName, LastName, Email, Phone, MailingStreet, MailingCity, MailingState,MailingCountry,MailingPostalCode, Class_Years__c, Preferred_Name__c, Alternate_Email__c, 
                    Family_Email__c, Financial_Aid_Email__c, Maryville_Email_Address__c, Preferred_Email__c, Work_Email__c, Maiden_Name__c FROM Contact 
                    WHERE Email =: mailId];
        
        lEvents = [SELECT Id, Name, Start_Date__c,Registration_Fields__c,Confirmed_Capacity_Reached__c,Number_of_Registrations__c,Allow_Waitlist__c,Max_Registration__c,Event_Description__c,Remaining_waitlist_count__c,Waitlisted_Capacity_Reached__c,Discount_Code__c,Food_Served__c,Allow_Donations__c,Allow_Guest__c, End_Date__c,Accept_Discount_Code__c, Event_Location__c, Event_Title__c, Registration_Fee__c, Event_Sub_Title__c, Event_Snippet__c, Event_Thank_You__c, 
                   OwnerId,Email_Signature_HTML__c, End_Registration__c,Event_Webinar_Link__c,Event_Address_Street__c,Event_Address_State__c,Event_Address_City__c  
                   FROM Event__c WHERE Name =: eventName];

        If(lEvents.size() > 0){
            Event_Registration__c registrationRecord = New Event_Registration__c();
            registrationRecord.First_Name__c = firstName;
            registrationRecord.Last_Name__c = lastName;
            
            If(MyMatcher.matches()){
                registrationRecord.Email__c = mailId;
                registrationRecord.Event__c = lEvents[0].Id;

                If(lContact.size() > 0){
                    registrationRecord.Contact__c = lContact[0].Id;
                }

                registrationRecord.Status__c = 'Registered';

                If(registrationRecord != null){
                    lRegistrations.add(registrationRecord);
                    insert registrationRecord;
                    ApexPages.Message msg = new ApexPages.message(ApexPages.severity.CONFIRM, 'Registration confirmed');
                    ApexPages.addMessage(msg);
                    sendRegistrationEmail();
                }
            }else{
                ApexPages.Message msg = new ApexPages.message(ApexPages.severity.ERROR, 'Please enter a valid mail id');
                ApexPages.addMessage(msg);
            }
        }else{
            ApexPages.Message msg = new ApexPages.message(ApexPages.severity.ERROR, 'Please enter a valid event name');
            ApexPages.addMessage(msg);
        }
        return null;
    }
    
    public void sendRegistrationEmail() {
        String location;
        DateTime startDate;
        DateTime endDate;

        string htmlBody = '<style>table {border-collapse: collapse; width: 100%; } th, td {border: 1px solid black;}</style>';
        htmlBody += '<font size="6" face="arial" color="#3f3f3f">Maryville University of St. Louis</font><hr/>';
        htmlBody += 'Dear ' + firstName + ', <br/><br/>';
        htmlBody += 'We look forward to seeing you at the '+lEvents[0].Name+'! The following registrations are confirmed: <br/><br/>';
        htmlBody += '<table><thead><tr><th><b>Registrant</b></th><th><b>Registered Events</b></th><th>Registration Fee</th></tr>';
        htmlBody += '<tr><td>' + firstName + ' ' + lastName + '</td>';
        htmlBody += '<td>' + lEvents[0].Name + '</td>';
        htmlBody += '<td>' + lEvents[0].Registration_Fee__c + '</td></tr>';

        If(lEvents[0].Event_Location__c == 'On-Campus'){
            roomRequestRecord = [SELECT Id,Event__c,Event__r.Name, Room__c,Room__r.Room_Number__c,Room__r.Building__c,Room__r.Building__r.Name, isPrimary__c FROM Room_Request__c WHERE Event__r.Name = : lEvents[0].Name AND isPrimary__c =: True ];
            
            If(roomRequestRecord.size() > 0){
                If((roomRequestRecord[0].Room__r.Building__r.Name != Null) && (roomRequestRecord[0].Room__r.Room_Number__c != Null)){
                    primaryBuildingName = roomRequestRecord[0].Room__r.Building__r.Name;
                    primaryRoomNumber = roomRequestRecord[0].Room__r.Room_Number__c;
                    location = lEvents[0].Event_Location__c +'-'+ primaryBuildingName+'-'+primaryRoomNumber;
                }
            }else{
                location = lEvents[0].Event_Location__c;
            }
        }
        Else if (lEvents[0].Event_Location__c == 'Off-Campus'){
            If(lEvents[0].Event_Address_Street__c != Null){
                location = lEvents[0].Event_Address_Street__c;
            }else{
                location = lEvents[0].Event_Location__c;
            }
        }
        Else if(lEvents[0].Event_Location__c == 'Virtual'){
            If(lEvents[0].Event_Webinar_Link__c != Null){
                location = lEvents[0].Event_Webinar_Link__c;
            }else{
                location = lEvents[0].Event_Location__c;
            }
        }
        Else{
            location = lEvents[0].Event_Location__c;
        }

        startDate = lEvents[0].Start_Date__c;
        endDate = lEvents[0].End_Date__c;

        if (lEvents[0].Registration_Fee__c > 0) {
            htmlBody += '<tfoot><tr><td><b>Total</b></td><td></td><td>$'+ lEvents[0].Registration_Fee__c + '</td></tr></tfoot>';
            htmlBody += '</table><br/><br/>';
            htmlBody += 'You have elected to pay the total of $' + lEvents[0].Registration_Fee__c + ' at the registered event. <br/>';
        }

        htmlBody += 'For more information, please navigate to <a href="https://www.maryville.edu/alumniweekend">this link</a>. <br/><br/>';
        
        if (String.isNotBlank(lEvents[0].Email_Signature_HTML__c))
            htmlBody += lEvents[0].Email_Signature_HTML__c;

        EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where name = 'Alumni Weekend Free Registration'];
        string templateHtmlBody = emailTemplate.Body.replace('TextToReplace', htmlBody);
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<String> tempToEmailIdsList = new List<String>();
        tempToEmailIdsList.add(mailId);
        email.setToAddresses(tempToEmailIdsList);
        email.setSaveAsActivity(true);
        email.setTemplateId(emailTemplate.Id);
        email.setSubject(eventName);
        email.setHtmlBody(templateHtmlBody);
        email.setPlainTextBody(templateHtmlBody);
        if(startDate != null && endDate != null){
            Date lDateGMT = startDate.dateGMT();
            Time lTimeGMT = startDate.timeGMT();
            
            Datetime lGMTNow = Datetime.newInstanceGmt(lDateGMT, lTimeGMT);
            String startDateGMT= String.valueOf(lGMTNow);
            startDateGMT=startDateGMT.replace('-','');
            startDateGMT=startDateGMT.replace(' ','');
            startDateGMT=startDateGMT.replace(':','');
            startDateGMT=startDateGMT+'Z';
            startDateGMT = startDateGMT.substring(0, 8)+'T'+startDateGMT.substring(8, startDateGMT.length());
            
            Date endDateGMT = endDate.dateGMT();
            Time endTimeGMT = endDate.timeGMT();
            Datetime GMTendDate = Datetime.newInstanceGmt(endDateGMT, endTimeGMT);
            String eDateGMT= String.valueOf(GMTendDate);
            eDateGMT=eDateGMT.replace('-','');
            eDateGMT=eDateGMT.replace(' ','');
            eDateGMT=eDateGMT.replace(':','');
            eDateGMT=eDateGMT+'Z';
            eDateGMT = eDateGMT.substring(0, 8)+'T'+eDateGMT.substring(8, eDateGMT.length());
            
            String[] icsTemplate = new List<String> {
                	'BEGIN:VCALENDAR',
                    'PRODID::-//hacksw/handcal//NONSGML v1.0//EN',
                    'VERSION:2.0',
                    'BEGIN:VEVENT',                                         
                    'DTSTART: '+startDateGMT,
                    'DTEND: ' +eDateGMT,
                    'LOCATION: ' +location,
                    'UID: ' +lEvents[0].Name,
                    'DESCRIPTION: ' +lEvents[0].Event_Description__c,
                    'SUMMARY: ' +lEvents[0].Name,
                    'BEGIN:VALARM',
                    'TRIGGER:-PT15M',
                    'ACTION:DISPLAY',
                    'DESCRIPTION:'+lEvents[0].Event_Description__c,
                    'END:VALARM',
                    'END:VEVENT',
                    'END:VCALENDAR'
            };
                        
            String attachment = String.join(icsTemplate, '\n');
            Messaging.EmailFileAttachment attach = New Messaging.EmailFileAttachment();
            attach.filename = 'Reminder.ics';
            
            attach.ContentType = 'text/Calender';
            //attach.inline = true;
            attach.body = Blob.valueOf(attachment);
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});
            //email.setFileAttachments(attach);
            
            list<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'alumni@maryville.edu'];
            if (owea.size() > 0) email.setOrgWideEmailAddressId(owea[0].Id);
            
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> { email };
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
    }
    
}