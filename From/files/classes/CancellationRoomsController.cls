public class CancellationRoomsController {

    String eventId;
    String roomId;
    public Boolean eventLevelPageOpened {get;set;}
    public Boolean roomLevelPageOpened {get;set;}
    public Boolean noDataFoundCase {get;set;}
    public Boolean selectAll {get;set;}
    public String isEventCreated {get; set;}
    public List<wrapper> listRoomWrapper {get;set;}
    public List<wrapper> listwrapper {get;set;}
    public List<wrapper> listEventWrapper {get;set;}
    public String isRoomCancelled {get; set;}
    Map<Id, Room_Request__c> filteredRoomsMap {get;set;}
    public String roomName {get;set;}

    public CancellationRoomsController()
    {
        roomLevelPageOpened = false;
        eventLevelPageOpened = false;
        noDataFoundCase = true;
        selectAll = false;

        roomId = ApexPages.currentPage().getParameters().get('roomId');
        eventId = ApexPages.currentPage().getParameters().get('eventId');

        if(!String.isBlank(roomId))
        {
            roomLevelPageOpened = true;
            noDataFoundCase = false;
            get_RoomsPlans(roomId);
        }
        else if(!String.isBlank(eventId))
        {
            eventLevelPageOpened = true;
            noDataFoundCase = false;
            get_RoomsPlans(eventId);
        }

        //listEventWrapper = get_RoomEventsPlans(eventId);
        
    }

    public void select_DeselectAll()
    {
        if(selectAll)
            for(wrapper listRoomWrapperVar : listRoomWrapper)
                listRoomWrapperVar.isSelected = true;
        else
            for(wrapper listRoomWrapperVar : listRoomWrapper)
                listRoomWrapperVar.isSelected = false;
    }

    // method to return all the room requests details
    public List<wrapper> get_RoomsPlans(Id eventId)
    {
        String query = 'SELECT Event__r.Name, Room__r.Name, Room__r.Room_Number__c, Status__c, Request_Type__c, Start_DateTime__c, End_DateTime__c FROM Room_Request__c ';
        
        IF(eventLevelPageOpened)
            query += 'WHERE Event__c = \'' + eventId + '\' ';
        ELSE IF(roomLevelPageOpened)
            query += 'WHERE Room__c = \'' + eventId + '\' ';

        query += 'AND Status__c NOT IN (\'Cancelled\', \'Rejected\')';

        List<Room_Request__c> tempRoomReqList = Database.query(query);
        if(tempRoomReqList.size() >0){
        if(roomLevelPageOpened)
            roomName = tempRoomReqList[0].Room__r.Name;
        else
            roomName = tempRoomReqList[0].Event__r.Name;

        filteredRoomsMap = new Map<Id, Room_Request__c>(tempRoomReqList);
        
        listRoomWrapper = new List<wrapper>();

        for(Room_Request__c filteredRoomsMapVar : filteredRoomsMap.values())
            listRoomWrapper.add(
                new wrapper(
                    filteredRoomsMapVar.Id,
                    filteredRoomsMapVar.Room__r.Name,
                    filteredRoomsMapVar.Room__r.Room_Number__c,
                    filteredRoomsMapVar.Event__r.Name,
                    filteredRoomsMapVar.Start_DateTime__c,
                    filteredRoomsMapVar.End_DateTime__c,
                    filteredRoomsMapVar.Request_Type__c,
                    filteredRoomsMapVar.Status__c,
                    false
                ));
        }
        return listRoomWrapper;
        
    }

    public Boolean cancelRoomRequests()
    {
        try
        {
            for(wrapper listRoomWrapperVar : listRoomWrapper)
                if(!listRoomWrapperVar.isSelected)
                    filteredRoomsMap.remove(listRoomWrapperVar.Id);
                else
                    filteredRoomsMap.get(listRoomWrapperVar.Id).Status__c = 'Cancelled';

            update filteredRoomsMap.values();
            isRoomCancelled = 'true';
            
            for(Room_Request__c filteredRoomsMapVar : filteredRoomsMap.values())
            {
                IF(!Approval.isLocked(filteredRoomsMapVar.Id))
                {
                    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                    req.setComments('Submitted for approval. Kindly Check.');
                    req.setObjectId(filteredRoomsMapVar.Id);
                    Approval.ProcessResult result = Approval.process(req);
                }
            }
        } catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
        }
        return null;
    }

    public class wrapper {
        public String Id {get;set;}
        public String roomName {get;set;}
        public String roomNumber {get;set;}
        public String eventName {get;set;}
        public Datetime startTime {get;set;}
        public Datetime endTime {get;set;}
        public String type {get;set;}
        public String roomStatus {get;set;}
        public Boolean isSelected {get;set;}
        
        public wrapper(String Id, String roomName, String roomNumber, String eventName, Datetime startTime, Datetime endTime, String type, String roomStatus, Boolean isSelected) {
            this.Id = Id;
            this.roomName = roomName;
            this.roomNumber = roomNumber;
            this.eventName = eventName;
            this.startTime = startTime;
            this.endTime = endTime;
            this.type = type;
            this.roomStatus = roomStatus;
            this.isSelected = isSelected;
        }
    }

    // method to return all the booked room details
   /* public List<wrapper> get_RoomEventsPlans(Id eventId)
    {
        Map<Id, Room_Booking__c> filteredRoomEventsMap = new Map<Id, Room_Booking__c>([SELECT Room_Request__c FROM Room_Booking__c
                                                                                    WHERE Room_Request__c IN
                                                                                        (SELECT Id from Room_Request__c
                                                                                            WHERE Event__c =: eventId
                                                                                            AND Status__c NOT IN ('Cancelled', 'Rejected'))]);
        /*List<wrapper> listEventWrapperTemp = new List<wrapper>();

        for(Room_Booking__c filteredRoomsMapVar : filteredRoomsMap.values())
        {
            listEventWrapperTemp.add(
                new wrapper(
                        filteredRoomsMapVar.Id,
                        filteredRoomsMapVar.Room__r.Name,
                        filteredRoomsMapVar.Room__r.Room_Number__c,
                        filteredRoomsMapVar.Room__r.Room_Number__c,
                        filteredRoomsMapVar.Start_Timeslot__c,
                        filteredRoomsMapVar.End_Timeslot__c,
                        'Booked',
                        false
                    ));
        }

        Set<Id> roomReqIds = new Set<Id>();
        for(Room_Booking__c filteredRoomEventsMapVar : filteredRoomEventsMap.values())
        {
            roomReqIds.add(filteredRoomEventsMapVar.Room_Request__c);
        }

        Map<Id, Room_Request__c> filteredRoomsMap = new Map<Id, Room_Request__c>([SELECT Event__r.Name,
                                                                                    Room__r.Name,
                                                                                    Room__r.Room_Number__c,
                                                                                    Status__c,
                                                                                    Request_Type__c,
                                                                                    Start_DateTime__c,
                                                                                End_DateTime__c FROM Room_Request__c
                                                                                    WHERE Id IN: roomReqIds]);

        List<wrapper> listRoomWrapperTemp = new List<wrapper>();

        for(Room_Request__c filteredRoomsMapVar : filteredRoomsMap.values())
        {
            listRoomWrapperTemp.add(
                new wrapper(
                        filteredRoomsMapVar.Id,
                        filteredRoomsMapVar.Room__r.Name,
                        filteredRoomsMapVar.Room__r.Room_Number__c,
                        filteredRoomsMapVar.Event__r.Name,
                        filteredRoomsMapVar.Start_DateTime__c,
                        filteredRoomsMapVar.End_DateTime__c,
                        filteredRoomsMapVar.Request_Type__c,
                        filteredRoomsMapVar.Status__c,
                        false
                    ));
        }

        return listRoomWrapperTemp;
    }*/

}