<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Initial_Event_Submission_Alert</fullName>
        <description>Initial Event Submission Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>kschmid1maryville@huronconsultinggroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Maryville_Approval_Templates/Maryville_event_Approval_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Status_Field_Update</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Field_Update_R</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
