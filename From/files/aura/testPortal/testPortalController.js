({
	myAction : function(component, event, helper) {
		var action = component.get("c.get_EventsList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.eventsList", returnValue );
            }
        });
        $A.enqueueAction(action);
        console.clear();
	},
    
    eventCreationHandler : function(component, event, helper) {
    	window.open('/s/event-creation', '_blank');
    	return false;
	},
    
    eventRegistrationCommunityHandler : function(component, event, helper) {
    	var elem = document.getElementsByClassName("eventsListSection")[0];
		elem.style.display = elem.style.display === 'none' ? '' : 'none';
    	return false;
	},
    
    eventRegistrationInPersonHandler : function(component, event, helper) {
    	window.open('/s/event-registration-in-person', '_blank');
    	return false;
	},
    
    eventListHandler : function(component, event, helper) {
    	window.open('/s/events-list', '_blank');
    	return false;
	},
    
    neweventListHandler : function(component, event, helper) {
    	window.open('/s/events-lists', '_blank');
    	return false;
	},
    
    eventCalendarHandler : function(component, event, helper) {
    	window.open('/s/calendar', '_blank');
    	return false;
	},

    eventRegistrationCommunityFinalHandler : function(component, event, helper) {
        var recordId = event.target.dataset.recordid;
        var isDiscountAllowed = event.target.dataset.isdiscountallowed;

        window.open('/s/event-registration-community?recordId='+recordId, '_blank');

        var action = component.get("c.get_DiscountCode");
        action.setParams({
            eventId : recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var discountId = response.getReturnValue();
                component.set("v.discountId", discountId );
                if(!String.isBlank(discountId))
                    window.open('/s/event-registration-community?DiscountId=' + discountId + '&recordId='+recordId, '_blank'); 
                else
                    window.open('/s/event-registration-community?recordId='+recordId, '_blank');
            }
        });
        $A.enqueueAction(action);
        return false;
    }
})