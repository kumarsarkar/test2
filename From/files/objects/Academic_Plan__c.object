<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Plans to help students who are in financial aid suspension status or appealing academic dismissal</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Academic_Dismissal_Appeal__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>The related student&apos;s academic appeal.</inlineHelpText>
        <label>Academic Dismissal Appeal</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Academic Plans (Academic Dismissal Appeal)</relationshipLabel>
        <relationshipName>Academic_Plans1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Academic Plans</relationshipLabel>
        <relationshipName>Academic_Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Details__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter some descriptive details about this student&apos;s plan. Use the milestones related list below to set specific requirements.</inlineHelpText>
        <label>Details</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Display_Name__c</fullName>
        <externalId>false</externalId>
        <label>Display Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SAP_Appeal__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>The related student&apos;s SAP appeal case</inlineHelpText>
        <label>SAP Appeal</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Academic Plans</relationshipLabel>
        <relationshipName>Academic_Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Not Started</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Completed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Failed</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Student__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Student</label>
        <referenceTo>Student__c</referenceTo>
        <relationshipLabel>Academic Plans</relationshipLabel>
        <relationshipName>Academic_Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Academic Plan</label>
    <nameField>
        <displayFormat>AP-{000000}</displayFormat>
        <label>Plan Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Academic Plans</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
