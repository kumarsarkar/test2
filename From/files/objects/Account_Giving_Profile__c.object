<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account Manager</label>
        <referenceTo>Account_Manager__c</referenceTo>
        <relationshipLabel>Account Giving Profiles</relationshipLabel>
        <relationshipName>Account_Giving_Profiles</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account Giving Profiles</relationshipLabel>
        <relationshipName>Account_Giving_Profiles</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>External_Id_Informatica__c</fullName>
        <externalId>true</externalId>
        <label>External Id(Informatica)</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Research__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Research Info</label>
        <referenceTo>Research_Info__c</referenceTo>
        <relationshipLabel>Account Giving Profiles</relationshipLabel>
        <relationshipName>Account_Giving_Profiles</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Distribution_Frequency_c__c</fullName>
        <description>Frequency with which the organization gives for matching gifts.</description>
        <externalId>false</externalId>
        <inlineHelpText>Frequency with which the organization gives for matching gifts.</inlineHelpText>
        <label>Distribution Frequency</label>
        <picklist>
            <picklistValues>
                <fullName>Monthly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quarterly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Semi-Annually</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Annually</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Installments_Allowed_c__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Installment pledges allowed for matching?</description>
        <externalId>false</externalId>
        <inlineHelpText>Installment pledges allowed for matching?</inlineHelpText>
        <label>Installments Allowed?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Items_Allowed_c__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Items allowed on matching pledges?</description>
        <externalId>false</externalId>
        <inlineHelpText>Items allowed on matching pledges?</inlineHelpText>
        <label>Items Allowed?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Match_Ratio_c__c</fullName>
        <externalId>false</externalId>
        <label>Match Ratio</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Matching_Ratio_Type_c__c</fullName>
        <description>Displays the Primary Matching Gift type for the Account. Value is updated based on MatchingRatio_UpdatePrimary , which pulls &apos;Matching Ratio Type’ value from linked Matching Ratio record where ‘Primary&apos; is selected.</description>
        <externalId>false</externalId>
        <inlineHelpText>Displays the Primary Matching Gift type for the Account. Value is updated based on &apos;Matching Ratio Type&apos; field from Matching Ratio record where ‘Primary’ field is selected.</inlineHelpText>
        <label>Matching Ratio Type</label>
        <picklist>
            <picklistValues>
                <fullName>Current Employee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Director</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Executive</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Retiree</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Spouse</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Maximum_Matched_c__c</fullName>
        <externalId>false</externalId>
        <label>Maximum Matched</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>rC_Giving_Minimum_Matched_c__c</fullName>
        <externalId>false</externalId>
        <label>Minimum Matched</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Account Giving Profile</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>AGP-{0000}</displayFormat>
        <label>Account Giving Profile Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Giving Profiles</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
