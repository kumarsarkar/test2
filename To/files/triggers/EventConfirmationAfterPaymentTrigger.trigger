/*

// Commented & Added new optimized code, Kumar Sarkar, 11-27-17

trigger EventConfirmationAfterPaymentTrigger on pymt__PaymentX__c (after update) {
    
        if(Trigger.isAfter){
            if(Trigger.isUpdate){
                sendRegistrationEmail();
            }
        }

    
        Public void sendRegistrationEmail() {
        
            String eventName;
            String eventDesription;
            DateTime startDate;
            DateTime endDate;
            String location;
            String primaryBuildingName;
            String primaryRoomNumber;
            List<Room_Request__c> roomRequestRecord = New List<Room_Request__c>();
            // collect all payments that have just changed in status to "completed"
            
            list<Id> paymentIds = new list<Id>();
            for (pymt__PaymentX__c pymt : Trigger.New) {
                if (pymt.pymt__Status__c == 'Completed') {
                    paymentIds.add(pymt.Id);
                }
            }
            
            list<String> paymentNameList = new list<String>();
            for (pymt__PaymentX__c pymt : Trigger.New) {
                String paymentName = pymt.Name;
               paymentName = paymentName.substring(0,paymentName.length()- 13);
               paymentNameList.add(paymentName);
            }
            
            
            /*String paymentName = Trigger.New[0].Name ;
            paymentName= paymentName.substring(0,paymentName.length()- 13);
            system.debug('$$$$paymentName  '+paymentName);
            
            
            
            // collect all event registrations that belong to these payments, where contact is not null
            List<Event_Registration__c> registrantDetails = [SELECT Id, Contact__c,Contact__r.FirstName,Contact__r.LastName, Event__c,First_Name__c,Last_Name__c FROM Event_Registration__c WHERE Payment__c IN :paymentIds LIMIT 1];
            
            List<Event__c> eventDetails = [SELECT Id,Name,Start_Date__c,End_Date__c,Event_Description__c,Event_Location__c,Registration_Fee__c,Event_Address_Street__c,Event_Address_State__c,Event_Address_City__c,Event_Webinar_Link__c FROM Event__c WHERE Name IN : paymentNameList]; 
            
            for(Event__c evt : eventDetails ){
                string htmlBody = '<style>table {border-collapse: collapse; width: 100%; } th, td {border: 1px solid black;}</style>';
            htmlBody += '<font size="6" face="arial" color="#3f3f3f">Maryville University of St. Louis</font><hr/>';
            
            if(registrantDetails.size() >0){
                
                htmlBody += 'Dear ' + registrantDetails[0].First_Name__c + ', <br/><br/>';
                htmlBody += 'We look forward to seeing you at '+eventDetails[0].Name +  'event.The following registrations are confirmed: <br/><br/>';
                htmlBody += '<table><thead><tr><th><b>Registrant</b></th><th><b>Registered Events</b></th><th>Registration Fee</th></tr>';
                htmlBody += '<tr><td>' + registrantDetails[0].Contact__r.FirstName + ' ' + registrantDetails[0].Contact__r.LastName + '</td>';
                
                htmlBody += '<td>' + eventDetails[0].Name + '</td>';
                eventName = eventDetails[0].Name;
                If(eventDetails[0].Event_Location__c == 'On-Campus'){
                            roomRequestRecord = [SELECT Id,Event__c,Event__r.Name, Room__c,Room__r.Room_Number__c,Room__r.Building__c,Room__r.Building__r.Name, isPrimary__c FROM Room_Request__c WHERE Event__r.Name = : eventDetails[0].Name AND isPrimary__c =: True LIMIT 1 ];
                        
                        If(roomRequestRecord.size() > 0){
                            If((roomRequestRecord[0].Room__r.Building__r.Name != Null) && (roomRequestRecord[0].Room__r.Room_Number__c != Null)){
                        primaryBuildingName = roomRequestRecord[0].Room__r.Building__r.Name;
                        primaryRoomNumber = roomRequestRecord[0].Room__r.Room_Number__c;
                        location = eventDetails[0].Event_Location__c +'-'+ primaryBuildingName+'-'+primaryRoomNumber;
                             }
                        }else{
                            location = eventDetails[0].Event_Location__c;
                        }
                        }
                        Else if (eventDetails[0].Event_Location__c == 'Off-Campus'){
                            If((eventDetails[0].Event_Address_Street__c != Null) &&(eventDetails[0].Event_Address_State__c!= Null) && (eventDetails[0].Event_Address_City__c  != Null)){
                                location = eventDetails[0].Event_Address_Street__c+'-'+eventDetails[0].Event_Address_State__c+'-'+eventDetails[0].Event_Address_City__c  ;
                            }else{
                                location = eventDetails[0].Event_Location__c;
                            }
                            
                        }
                        Else if(eventDetails[0].Event_Location__c == 'Virtual'){
                            If(eventDetails[0].Event_Webinar_Link__c != Null){
                                location = eventDetails[0].Event_Webinar_Link__c;
                            }else{
                                location = eventDetails[0].Event_Location__c;
                            }
                        }
                        Else{
                            location = eventDetails[0].Event_Location__c;
                        }
        
                //location = eventDetails[0].Event_Location__c;
                startDate = eventDetails[0].Start_Date__c;
                endDate = eventDetails[0].End_Date__c;
                eventDesription = eventDetails[0].Event_Description__c;
                //String connectionId = ApexPages.currentPage().getParameters().get('connectionId');
                String connectionId = '';
                
                if (eventDetails[0].Registration_Fee__c == 0) htmlBody += '<td>FREE</td></tr>';
                else htmlBody += '<td>$' + eventDetails[0].Registration_Fee__c + '</td></tr>';
                htmlBody += '<tfoot><tr><td><b>Total</b></td><td></td><td>$'+ Trigger.New[0].pymt__Amount__c+ '</td></tr></tfoot>';
                htmlBody += '</table><br/><br/>';
                
                htmlBody += 'You have elected to pay the total of $' + eventDetails[0].Registration_Fee__c + ' at '+ eventDetails[0].Name + ' event. <br/>';
                //htmlBody += 'For more information regarding Alumni Weekend, please navigate to <a href="https://www.maryville.edu/alumniweekend">this link</a>. <br/><br/>';
                htmlBody += 'For more information, please navigate to <a href="http://develop-l-maryville.cs19.force.com/pmtx/apex/EventRegistration?Id='+evt .id+'&connectionId='+connectionId+'">this link</a><br/><br/>';
                EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where name = 'Alumni Weekend Free Registration'];
                string templateHtmlBody = emailTemplate.Body.replace('TextToReplace', htmlBody);
                system.debug('--- htmlbody: ' + templateHtmlBody);
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(registrantDetails[0].Contact__r.Id);
                email.setSaveAsActivity(true);
                email.setTemplateId(emailTemplate.Id);
                //email.setSubject(emailTemplate.Subject);
                email.setSubject(eventName);
                email.setHtmlBody(templateHtmlBody);
                email.setPlainTextBody(templateHtmlBody);
                
                //email attachment
                
                system.debug('@@@startDate'+startDate);
                system.debug('@@@endDate'+endDate);
                if(startDate != null && endDate != null){
                    Date lDateGMT = startDate.dateGMT();
                    //Date lDateGMT = Date.valueof(startDate);
                    Time lTimeGMT = startDate.timeGMT();
                    
                    Datetime lGMTNow = Datetime.newInstanceGmt(lDateGMT, lTimeGMT);
                    String startDateGMT= String.valueOf(lGMTNow);
                    startDateGMT=startDateGMT.replace('-','');
                    startDateGMT=startDateGMT.replace(' ','');
                    startDateGMT=startDateGMT.replace(':','');
                    startDateGMT=startDateGMT+'Z';
                    startDateGMT = startDateGMT.substring(0, 8)+'T'+startDateGMT.substring(8, startDateGMT.length());
                    
                    Date endDateGMT = endDate.dateGMT();
                    Time endTimeGMT = endDate.timeGMT();
                    Datetime GMTendDate = Datetime.newInstanceGmt(endDateGMT, endTimeGMT);
                    String eDateGMT= String.valueOf(GMTendDate);
                    eDateGMT=eDateGMT.replace('-','');
                    eDateGMT=eDateGMT.replace(' ','');
                    eDateGMT=eDateGMT.replace(':','');
                    eDateGMT=eDateGMT+'Z';
                    eDateGMT = eDateGMT.substring(0, 8)+'T'+eDateGMT.substring(8, eDateGMT.length());
                    
                    
                    String [] icsTemplate = new List<String> {
                            'BEGIN:VCALENDAR',
                            'PRODID::-//hacksw/handcal//NONSGML v1.0//EN',
                            'VERSION:2.0',
                            'BEGIN:VEVENT',                                         
                            'DTSTART: '+startDateGMT,
                            'DTEND: ' +eDateGMT,
                            'LOCATION: ' +location,
                            'UID: ' +eventName,
                            'DESCRIPTION: ' +eventDesription,
                            'SUMMARY: ' +eventName,
                            'BEGIN:VALARM',
                            'TRIGGER:-PT15M',
                            'ACTION:DISPLAY',
                            'DESCRIPTION:'+eventDesription,
                            'END:VALARM',
                            'END:VEVENT',
                            'END:VCALENDAR'
                            };
                     
                     String attachment = String.join(icsTemplate, '\n');
                    
                     Messaging.EmailFileAttachment attach = New Messaging.EmailFileAttachment();
                     attach.filename = 'Reminder.ics';
                    
                     attach.ContentType = 'text/Calender';
                     attach.inline = true;
                     attach.body = Blob.valueOf(attachment);
                     email.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});
                     //email.setFileAttachments(attach);
                    
                     list<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'alumni@maryville.edu'];
                     if (owea.size() > 0) email.setOrgWideEmailAddressId(owea[0].Id);
                    
                     Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> { email };
                     Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    
                     if (results[0].success) {
                        System.debug('The email was sent successfully.');
                     } else {
                        System.debug('The email failed to send: ' + results[0].errors[0].message);
                     }
                    
            }
        }
            }
            
    }
}*/

trigger EventConfirmationAfterPaymentTrigger on pymt__PaymentX__c (after update) {
    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            sendRegistrationEmail();
        }
    }

    Public void sendRegistrationEmail() {
        String eventName;
        String eventDesription;
        DateTime startDate;
        DateTime endDate;
        String location;
        String primaryBuildingName;
        String primaryRoomNumber;
        List<Room_Request__c> roomRequestRecord = New List<Room_Request__c>();
        Map<pymt__PaymentX__c, Map<Event__c, List<Event_Registration__c>>> payment_Event_Registrant_Details = new Map<pymt__PaymentX__c, Map<Event__c, List<Event_Registration__c>>>();

        Map<Id, String> paymentNameIDMap = new Map<Id, String>();
        for(pymt__PaymentX__c pymt : Trigger.New)
        {
            String paymentName = pymt.Name;
            paymentName = paymentName.substring(0,paymentName.length()- 13);
            paymentNameIDMap.put(pymt.Id, paymentName);
        }

        List<Event_Registration__c> registrantDetails = [SELECT Id,
                                                            Name,
                                                            Contact__c,
                                                            Contact__r.FirstName,
                                                            Contact__r.LastName,
                                                            Event__c,
                                                            Event__r.Name,
                                                            First_Name__c,
                                                            Last_Name__c,
                                                        Payment__c FROM Event_Registration__c
                                                        WHERE Payment__c IN: paymentNameIDMap.keySet()
                                                        ORDER BY Payment__c];
        
        Set<Id> eventIds = new Set<Id>();

        for(Event_Registration__c registrantDetailsVar : registrantDetails)
            eventIds.add(registrantDetailsVar.Event__c);

        List<Event__c> eventsList = [SELECT Id,
                                        Name,
                                        Start_Date__c,
                                        End_Date__c,
                                        Event_Description__c,
                                        Event_Location__c,
                                        Registration_Fee__c,
                                        Event_Address_Street__c,
                                        Event_Address_State__c,
                                        Event_Address_City__c,
                                        Event_Webinar_Link__c,
                                        GMT_Start_Date__c,
                                    GMT_End_Date__c FROM Event__c
                                    WHERE Id IN : eventIds];

        for(pymt__PaymentX__c pymt : Trigger.New)
        {
            List<Event_Registration__c> tempRegistrantList = new List<Event_Registration__c>();
            Event__c tempMainEvent = new Event__c();
            List<Event__c> tempEventDetails = new List<Event__c>();

            for(Event_Registration__c registrantDetailsVar : registrantDetails)
            {
                if(registrantDetailsVar.Payment__c == pymt.Id)
                    tempRegistrantList.add(registrantDetailsVar);
            }

            for(Event__c eventsListVar : eventsList)
            {
                if(eventsListVar.Name == paymentNameIDMap.get(pymt.Id))
                {
                    tempMainEvent = eventsListVar;
                    break;
                }
            }

            Map<Event__c, List<Event_Registration__c>> temp = new Map<Event__c, List<Event_Registration__c>>();
            temp.put(tempMainEvent, tempRegistrantList);
            payment_Event_Registrant_Details.put(pymt, temp);
        }

        for(pymt__PaymentX__c pymt : Trigger.New)
        {
            string htmlBody = '<style> table {border-collapse: collapse;} </style>';
            htmlBody += '<font size="6" face="arial" color="#3f3f3f">Maryville University of St. Louis</font><hr/>';
            
            if(registrantDetails.size() >0){
                
                Event__c mainEvent = new Event__c();
                for(Event__c temp : payment_Event_Registrant_Details.get(pymt).keyset())
                    mainEvent = temp;
                Event_Registration__c singleEmailRegistrantDetail = payment_Event_Registrant_Details.get(pymt).values()[0][0];

                htmlBody += 'Dear ' + singleEmailRegistrantDetail.First_Name__c + ', <br/><br/>';
                htmlBody += 'We look forward to seeing you at the "'+mainEvent.Name+'". The following registrations are confirmed: <br/><br/>';
                htmlBody += '<table><thead><tr><th><b>Registrant</b></th><th><b>Registered Events</b></th>';
                
                for(Event_Registration__c tempRegistrars : payment_Event_Registrant_Details.get(pymt).values()[0])
                {
                    htmlBody += '<tr><td>' + tempRegistrars.First_Name__c + ' ' + tempRegistrars.Last_Name__c + '</td>';
                    htmlBody += '<td>' + tempRegistrars.Event__r.Name + '</td></tr>';
                }

                htmlBody += '</table><br/><br/>';
                htmlBody += 'You have paid a total of $' + Trigger.New[0].pymt__Amount__c + ' for '+ '"'+ mainEvent.Name + '"'+' event. <br/>';

                eventName = mainEvent.Name;

                If(mainEvent.Event_Location__c == 'On-Campus'){
                    roomRequestRecord = [SELECT Id,Event__c,Event__r.Name, Room__c,Room__r.Room_Number__c,Room__r.Building__c,Room__r.Building__r.Name, isPrimary__c FROM Room_Request__c WHERE Event__r.Name = : mainEvent.Name AND isPrimary__c =: True LIMIT 1 ];
                    
                    If(roomRequestRecord.size() > 0){
                        If((roomRequestRecord[0].Room__r.Building__r.Name != Null) && (roomRequestRecord[0].Room__r.Room_Number__c != Null)){
                            primaryBuildingName = roomRequestRecord[0].Room__r.Building__r.Name;
                            primaryRoomNumber = roomRequestRecord[0].Room__r.Room_Number__c;
                            location = mainEvent.Event_Location__c +'-'+ primaryBuildingName+'-'+primaryRoomNumber;
                        }
                    }else{
                        location = mainEvent.Event_Location__c;
                    }
                }
                Else if (mainEvent.Event_Location__c == 'Off-Campus'){
                    If((mainEvent.Event_Address_Street__c != Null) &&(mainEvent.Event_Address_State__c!= Null) && (mainEvent.Event_Address_City__c  != Null)){
                        location = mainEvent.Event_Address_Street__c+'-'+mainEvent.Event_Address_State__c+'-'+mainEvent.Event_Address_City__c  ;
                    }else{
                        location = mainEvent.Event_Location__c;
                    }
                    
                }
                Else if(mainEvent.Event_Location__c == 'Virtual'){
                    If(mainEvent.Event_Webinar_Link__c != Null){
                        location = mainEvent.Event_Webinar_Link__c;
                    }else{
                        location = mainEvent.Event_Location__c;
                    }
                }
                Else{
                    location = mainEvent.Event_Location__c;
                }
                
                String connectionId = ApexPages.currentPage().getParameters().get('connectionId');

                //COMMNETED NOW FOR LATER USE, BY Kumar Sarkar, 12-13-17, htmlBody += 'For more information, please navigate to <a href="' + Label.Event_Community_URL + '/pmtx/apex/EventRegistration?Id='+ mainEvent.id +'&connectionId='+connectionId+'">this link</a><br/><br/>';
                htmlBody += 'For more information, please navigate to <a href="' + Label.Event_Community_URL + '/s/">this link</a><br/><br/>';
                EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where name = 'Alumni Weekend Free Registration'];
                string templateHtmlBody = emailTemplate.Body.replace('TextToReplace', htmlBody);
                system.debug('--- htmlbody: ' + templateHtmlBody);
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(singleEmailRegistrantDetail.Contact__r.Id);
                email.setSaveAsActivity(true);
                email.setTemplateId(emailTemplate.Id);
                email.setSubject(eventName);
                email.setHtmlBody(templateHtmlBody);
                email.setPlainTextBody(templateHtmlBody);
                
                startDate = mainEvent.Start_Date__c;
                endDate = mainEvent.End_Date__c;

                if(startDate != null && endDate != null){
                    /* COMMENTED & ADDED BY Kumar Sarkar, 12-07-17 [START]
                    Date lDateGMT = startDate.dateGMT();
                    //Date lDateGMT = Date.valueof(startDate);
                    Time lTimeGMT = startDate.timeGMT();
                    
                    Datetime lGMTNow = Datetime.newInstanceGmt(lDateGMT, lTimeGMT);
                    String startDateGMT= String.valueOf(lGMTNow);
                    startDateGMT=startDateGMT.replace('-','');
                    startDateGMT=startDateGMT.replace(' ','');
                    startDateGMT=startDateGMT.replace(':','');
                    startDateGMT=startDateGMT+'Z';
                    startDateGMT = startDateGMT.substring(0, 8)+'T'+startDateGMT.substring(8, startDateGMT.length());
                    
                    Date endDateGMT = endDate.dateGMT();
                    Time endTimeGMT = endDate.timeGMT();
                    Datetime GMTendDate = Datetime.newInstanceGmt(endDateGMT, endTimeGMT);
                    String eDateGMT= String.valueOf(GMTendDate);
                    eDateGMT=eDateGMT.replace('-','');
                    eDateGMT=eDateGMT.replace(' ','');
                    eDateGMT=eDateGMT.replace(':','');
                    eDateGMT=eDateGMT+'Z';
                    eDateGMT = eDateGMT.substring(0, 8)+'T'+eDateGMT.substring(8, eDateGMT.length());
                    */

                    String startDateGMT = mainEvent.GMT_Start_Date__c;
                    String eDateGMT = mainEvent.GMT_End_Date__c;

                    // COMMENTED & ADDED BY Kumar Sarkar, 12-07-17 [END]

                    String [] icsTemplate = new List<String> {
                        'BEGIN:VCALENDAR',
                            'PRODID::-//hacksw/handcal//NONSGML v1.0//EN',
                            'VERSION:2.0',
                            'BEGIN:VEVENT',                                         
                            'DTSTART:20171222T180000Z',
                            'DTEND:20171222T180000Z',
                            'LOCATION: ' +'location',
                            'UID: ' +'mainEvent.Name',
                            'DESCRIPTION: ' +'mainEvent.Event_Description__c',
                            'SUMMARY: ' +'mainEvent.Name',
                            'BEGIN:VALARM',
                            'TRIGGER:-PT15M',
                            'ACTION:DISPLAY',
                            'DESCRIPTION:'+'mainEvent.Event_Description__c',
                            'END:VALARM',
                            'END:VEVENT',
                            'END:VCALENDAR'
                            };
                                
                    String attachment = String.join(icsTemplate, '\\r\\n');
                    
                    Messaging.EmailFileAttachment attach = New Messaging.EmailFileAttachment();
                    attach.filename = 'Reminder.ics';
                    
                    attach.ContentType = 'text/Calender';
                    attach.inline = false;
                    attach.body = Blob.valueOf(attachment);
                    email.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});
                    //email.setFileAttachments(attach);
                    
                    list<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'alumni@maryville.edu'];
                    if (owea.size() > 0) email.setOrgWideEmailAddressId(owea[0].Id);
                    
                    Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> { email };
                        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    
                    if (results[0].success) {
                        System.debug('The email was sent successfully.');
                    } else {
                        System.debug('The email failed to send: ' + results[0].errors[0].message);
                    }
                }
            }
        }
    }
}