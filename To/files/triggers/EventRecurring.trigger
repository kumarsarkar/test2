trigger EventRecurring on Event__c (before update) {

    for(Event__c e : Trigger.New){
        
        If(e.Weekly__c == false){
            e.Sunday__c = false;
            e.Monday__c = false;
            e.Tuesday__c = false;
            e.Wednesday__c = false;
            e.Thursday__c = false;
            e.Friday__c = false;
            e.Saturday__c = false;
            
            
        
        }
        
        If(e.Monthly__c == false){
            
            e.Every_Month__c = '';
            e.Day__c = '';
            e.Day_of__c = '';
            
        
        }
        
        If(e.Yearly__c == false){
        
            e.Every_Year__c = '';
            e.Day1__c = '';
            e.Day_of1__c = ''; 
            
        
        }
        
        
    }
}