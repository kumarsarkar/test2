trigger updateEventLocationWithPrimaryRoomDetails on Room_Request__c (after insert,after update) {
    
    if(Trigger.isAfter){
    System.debug('inside trigger');
            if((Trigger.isInsert)||(Trigger.isUpdate)){
                System.debug('calling trigger');
                updateEventLocation();
            }
        }
    
    Public void updateEventLocation(){
        List<Id> lRoomRequestList = New List<Id>();
        List<Id> lEventIdList = New List<Id>();
        System.debug('Trigger.New'+Trigger.New);
         for (Room_Request__c roomRequest : Trigger.New) {
             
                if (roomRequest.isPrimary__c == true){
                    lRoomRequestList.add(roomRequest.Id);
                    lEventIdList.add(roomRequest.Event__c);
                }
          }
        
        
       List<Event__c> lEventDetails = New List<Event__c>();
        List<Room_Request__c> lRoomDetails = New List<Room_Request__c>();
        
       
        If(lRoomRequestList.size()>0){
            lRoomDetails = [SELECT Id,Room__c,Room__r.Room_Number__c,Room__r.Building__c,Room__r.Building__r.Name FROM Room_Request__c WHERE Id =: lRoomRequestList[0]];
            List<Room__c> roomslist = [SELECT Id,Name,Room_Number__c,Building__c,Building__r.Name from Room__c Where Id =: lRoomDetails[0].Room__c];
        
        List<Building__c> lBuilding = [SELECT Id,Name FROM Building__c WHERE Id =: roomslist[0].Building__c ];
       
        lEventDetails = [Select Id,Name,Event_Location__c FROM Event__c WHERE Id IN : lEventIdList AND Event_Location__c =: 'On-Campus'];  
       
        If((lRoomRequestList.size() > 0) && (lEventDetails.size()>0) && (lBuilding[0].Name != Null) && (roomslist[0].Room_Number__c != Null) ){
            
            lEventDetails[0].Primary_Room_Details__c = lBuilding[0].Name+'-'+roomslist[0].Room_Number__c;
            System.debug('lEventDetails[0].Primary_Room_Details__c'+lEventDetails[0].Primary_Room_Details__c);
           
                 
        }        
        update lEventDetails;
        }
        
    }
    

}