trigger RoomRequestTrigger on Room_Request__c (after update,after insert) {
    List<Room_Request__c> lstRoomReq = new List<Room_Request__c>();
    List<Room_Request__c> lstRoomReqToCancel = new List<Room_Request__c>();
    List<Room_Request__c> lstRoomReqInserted = new List<Room_Request__c>();
    List<Room_Request__c> lstRoomReqRejected = new List<Room_Request__c>();
    
    if (Trigger.isafter && Trigger.isupdate){
        
        //Call RoomRequestTriggerHandler only when Status is updated   
        for(Room_Request__c recRoomReq : Trigger.new)
        {
            if((Trigger.oldMap.get(recRoomReq.Id).Status__c != Trigger.newMap.get(recRoomReq.Id).Status__c) && (Trigger.oldMap.get(recRoomReq.Id).Status__c != 'Approved'))
            {
                lstRoomReq.add(recRoomReq);
            }
            if((Trigger.oldMap.get(recRoomReq.Id).Status__c != Trigger.newMap.get(recRoomReq.Id).Status__c) && (Trigger.oldMap.get(recRoomReq.Id).Status__c != 'Rejected'))
            {
                lstRoomReqRejected.add(recRoomReq);
            }
            if((Trigger.newMap.get(recRoomReq.Id).Status__c == 'Cancelled')  && ((Trigger.oldMap.get(recRoomReq.Id).Status__c == 'Pending Approval')))
            {
                /* COMMENTED BY Kumar Sarkar, 12-07-12
                    List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: recRoomReq.id];
                    Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                    req.setAction('Removed');        
                    req.setWorkitemId(piwi.get(0).Id);
                    Approval.process(req,false);
                */
            } 
            if((Trigger.oldMap.get(recRoomReq.Id).Status__c != Trigger.newMap.get(recRoomReq.Id).Status__c) && (Trigger.oldMap.get(recRoomReq.Id).Status__c != 'Cancelled'))
            {
                lstRoomReqToCancel.add(recRoomReq);
            }
        }
        if(!lstRoomReq.IsEmpty()){
            RoomRequestTriggerHandler.UpdateRoomAvailability(Trigger.new);
        }
        if(!lstRoomReqToCancel.IsEmpty()){
            RoomRequestTriggerHandler.CancelRoomAvailability(Trigger.new);
        }
        if(!lstRoomReqRejected.IsEmpty()){
            RoomRequestTriggerHandler.RejectRoomAvailability(Trigger.new,Trigger.old);
        }
    }
    
    if (Trigger.isafter && Trigger.isinsert){
        for(Room_Request__c recRoomReq : Trigger.new)
        {
            if((Trigger.newMap.get(recRoomReq.Id).Status__c == 'Pending Approval'))
            {
                lstRoomReqInserted.add(recRoomReq);
            }
        }
        if(!lstRoomReqInserted.IsEmpty()){
            RoomRequestTriggerHandler.ConfirmRoomAvailability(Trigger.new);
        }
    }
    
    if (Trigger.isafter){
        if((Trigger.isInsert)||(Trigger.isUpdate)){
            List<Id> lRoomRequestList = New List<Id>();
            List<Id> lEventIdList = New List<Id>();
            System.debug('Trigger.New'+Trigger.New);
            for (Room_Request__c roomRequest : Trigger.New) {
                if (roomRequest.isPrimary__c == true){
                    lRoomRequestList.add(roomRequest.Id);
                    lEventIdList.add(roomRequest.Event__c);
                }
            }
            List<Event__c> lEventDetails = New List<Event__c>();
            List<Room_Request__c> lRoomDetails = New List<Room_Request__c>();
            If(lRoomRequestList.size()>0){
                lRoomDetails = [SELECT Id,Room__c,Room__r.Room_Number__c,Room__r.Building__c,Room__r.Building__r.Name FROM Room_Request__c WHERE Id =: lRoomRequestList[0]];
                List<Room__c> roomslist = [SELECT Id,Name,Room_Number__c,Building__c,Building__r.Name from Room__c Where Id =: lRoomDetails[0].Room__c];
                List<Building__c> lBuilding = [SELECT Id,Name FROM Building__c WHERE Id =: roomslist[0].Building__c ];
                lEventDetails = [Select Id,Name,Event_Location__c FROM Event__c WHERE Id IN : lEventIdList AND Event_Location__c =: 'On-Campus'];
                
                If(!lBuilding.isEmpty() && !lEventDetails.isEmpty() && !roomslist.isEmpty())
                {
                    If((lBuilding[0].Name != Null) && (roomslist[0].Room_Number__c != Null) ){
                        lEventDetails[0].Primary_Room_Details__c = lBuilding[0].Name+'-'+roomslist[0].Room_Number__c;
                    }
            	}
                update lEventDetails;
            }
        }
    } 
}