public class EventListController {
    public List<Event_Registration__c> getMyRegistrations() {
        return [SELECT Event__c,Event__r.Name,Payment_Amount__c,Registered_Date__c,CreatedBy.Username
                FROM Event_Registration__c
                WHERE CreatedBy.Username =:UserInfo.getUsername()  ];
    }
    
    // ADDED BY Kumar Sarkar, 12-11-17 [START]
    public  List<Event_Registration__c> get_MyRegistrations() {
        return [SELECT Event__c,
                Event__r.Name,
                Payment_Amount__c,
                Registered_Date__c,
                CreatedBy.Username
                FROM Event_Registration__c
                WHERE CreatedBy.Username =:UserInfo.getUsername()  ];
    }
    // ADDED BY Kumar Sarkar, 12-11-17 [END]
}