public class AvailableRoomsController
{
    public Date dat {get;set;}
    public List<wrapper> listwrapper1 {get;set;}    // list of rooms available for a given filter criteria
    public List<wrapper> listwrapper2 {get;set;}   // list of rooms not available for a given filter criteria
    public List<wrapper> listwrapper {get;set;} 
    public Map<integer,wrapper> mapWrapper {get;set;}
    public String isEventCreated {get; set;}
    public Boolean openedInLightningComponent {get;set;}
    public Date eventStartDate {get;set;}
    public Date eventEndDate {get;set;}
    public string eventStartTime {get;set;}
    public String eventEndTime {get;set;}
    
    public List<SelectOption> eventDateList {get;set;}
    public list<Selectoption> CategoiesFieldList {get;set;}
    public list<Selectoption> SubFieldList {get;set;}
    public Boolean isRecurringEvent {get;set;}
    public Boolean bulkReservation {get;set;}
    public String eventName {get;set;}
    public list<dateTime> startList = new list<dateTime>();
    public list<dateTime> endList  = new list<dateTime>();
    public set<string> rmIdSEt  = new set<string>();
    public set< string> buildingList      = new set<string>();
    public set< string> typeList          = new set<string>();
    public string selectedField  {get;set;}
    public string SelectedCategoiesField {get;set;}
    List<Room_Request__c> roomRequestsList {get;set;}
    public dateTime startDATE;
    public datetime endDATE;
    public String retUrl;
    public String eventId {get;set;}
    public  String fullRecordURL {get;set;}
    Set<ID> roomIDSet {get;set;}
    
    public AvailableRoomsController()
    {
        roomIDSet = new Set<Id>();
        retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        eventId = ApexPages.currentPage().getParameters().get('eventId');
        fullRecordURL = 'https://maryvillesso--develop.cs19.my.salesforce.com/a2v29000000WYCj';
        bulkReservation = false;
        
        if(!String.isBlank(retUrl))
        {
            openedInLightningComponent = false;
            eventId = retUrl;
        }
        else
        {
            openedInLightningComponent = true;
        }
        
        Event__c selectedEvent = [SELECT Id, Name, Event_Setup_Start_Time__c, Event_Setup_End_Time__c, Start_Date__c, End_Date__c, Recurring__c FROM Event__c where Id =: eventId];
        // system.debug('selectedEvent ' + selectedEvent); 
        
        if(selectedEvent.Event_Setup_Start_Time__c == null && selectedEvent.Event_Setup_End_Time__c == null ){
            eventStartTime = selectedEvent.Start_Date__c.format('hh:mm a');
            eventEndTime = selectedEvent.End_Date__c.format('hh:mm a');
            eventStartDate = date.newinstance(selectedEvent.Start_Date__c.year(), selectedEvent.Start_Date__c.month(), selectedEvent.Start_Date__c.day());
            eventEndDate = date.newinstance(selectedEvent.End_Date__c.year(), selectedEvent.End_Date__c.month(), selectedEvent.End_Date__c.day());   
        }else{
            eventStartTime = selectedEvent.Event_Setup_Start_Time__c.format('hh:mm a');
            eventEndTime = selectedEvent.Event_Setup_End_Time__c.format('hh:mm a');
            eventStartDate = date.newinstance(selectedEvent.Event_Setup_Start_Time__c.year(), selectedEvent.Event_Setup_Start_Time__c.month(), selectedEvent.Event_Setup_Start_Time__c.day());
            eventEndDate = date.newinstance(selectedEvent.Event_Setup_End_Time__c.year(), selectedEvent.Event_Setup_End_Time__c.month(), selectedEvent.Event_Setup_End_Time__c.day());    
        }
        
        CategoiesFieldList = new List<SelectOption>();
        CategoiesFieldList.add(new SelectOption('-','-'));
        CategoiesFieldList.add(new SelectOption('TYPE','Type'));
        CategoiesFieldList.add(new SelectOption('BUILDING','Building'));
        // eventStartTimeOrg = selectedEvent.Start_Date__c.time();
        // eventEndTimeOrg = selectedEvent.End_Date__c.time();
        list<Room__c>             eList = [select id,Building__r.Name,Type__c from Room__c];
        for(Room__c ev : eList ){
            if(ev.Type__c <> null){
                typeList.add(ev.Type__c);
            }
            if(ev.building__r.Name <> null){
                buildingList.add(ev.building__r.Name);
            }
        }
        eventName = selectedEvent.Name;
        
        system.debug('11-24-17 callevent called');
        callevent();
        system.debug('11-24-17 callevent called.');
        
        if(selectedEvent.Recurring__c)
            isRecurringEvent = true;
        else
            isRecurringEvent = false;
    }

    public void callevent(){
        system.debug('insertCalss');
        
        // string sdate = eventStartDate.format();
        // string dDate = eventEndDate.format();
        if(!String.isBlank(eventStartTime) && !String.isBlank(eventEndTime) && eventStartDate != null && eventEndDate != null)
        {
            startDATE = convertStrinGtodate(eventStartTime,eventStartDate);
            endDATE = convertStrinGtodate(eventEndTime,eventEndDate);
            GetDateList evnt = new GetDateList();

            evnt.pageLoad(eventId,startDATE,endDATE);
            
            startList = evnt.startdateList;
            endList   = evnt.endDateList;
            
            system.debug('11-24-17 startList = ' + startList);
            system.debug('11-24-17 endList = ' + endList);

            for(integer i = 0 ; i<startList.size() ;i++ ){
                rmIdSEt.addAll(get_Rooms(startList[i], endList[i]));
            }

            get_Rooms1(rmIdSEt);
            system.debug('11-24-17 startList = ' + startList);
            system.debug('11-24-17 endList = ' + endList);
        }
    }

    public dateTime convertStrinGtodate( string selectedTime,date selectedDate){
        // date Sdate1 =   Date.parse(selectedDate);
        dateTime Sdate = selectedDate;
        system.debug('Sdate  ' + Sdate);
        
            if(Integer.valueOf(selectedTime.split(':')[0]) == 12){
                if(selectedTime.split(' ')[1].toUpperCase() == 'AM'){
                Sdate = Sdate.addMinutes(Integer.valueOf(selectedTime.split(':')[1].split(' ')[0]) );
                }else if(selectedTime.split(' ')[1].toUpperCase() == 'PM'){
                Sdate = Sdate.addMinutes(60*(Integer.valueOf(selectedTime.split(':')[0])) + Integer.valueOf(selectedTime.split(':')[1].split(' ')[0]) );
            }
            }else{
                if(selectedTime.split(' ')[1].toUpperCase() == 'AM'){
                Sdate = Sdate.addMinutes(60*Integer.valueOf(selectedTime.split(':')[0]) + Integer.valueOf(selectedTime.split(':')[1].split(' ')[0]) );
                }else if(selectedTime.split(' ')[1].toUpperCase() == 'PM'){
                Sdate = Sdate.addMinutes(60*(Integer.valueOf(selectedTime.split(':')[0])) + Integer.valueOf(selectedTime.split(':')[1].split(' ')[0])+720 );
            } 
            }
        
        
        Integer offset = UserInfo.getTimezone().getOffset(Sdate);
        dateTime startDt = Sdate.addSeconds(-1*offset/1000);
        return startDt;
    }
    public void saveRoomRequests()
    {
        try
        {
            integer dateSize   = startList.size();
            isEventCreated = 'false';
            roomRequestsList = new List<Room_Request__c>();
            for(wrapper listwrapperVar : listwrapper)
            {
                if(listwrapperVar.isSelected && listwrapperVar.isAvailable)
                {
                    
                    for(integer i = 0; i<dateSize; i++ ){
                        Room_Request__c tempRoomRequestRec = new Room_Request__c();
                        tempRoomRequestRec.Room__c = listwrapperVar.Id;
                        tempRoomRequestRec.Event__c = eventId;
                        tempRoomRequestRec.Status__c = 'Pending Approval';
                        tempRoomRequestRec.Request_Type__c = 'Reservation';
                        tempRoomRequestRec.Requested_By__c = UserInfo.getUserId();
                        tempRoomRequestRec.Start_DateTime__c = startList[i];
                        tempRoomRequestRec.End_DateTime__c = endList[i];
                        // system.debug('strt@@@@@  ' + startList[i]); 
                        roomRequestsList.add(tempRoomRequestRec);
                        system.debug('roomRequestsList  ' + roomRequestsList);
                    }
                }
            }
            if(!roomRequestsList.isEmpty())
            {
                insert roomRequestsList;
                isEventCreated = 'true';
            }
            /**   if(isEventCreated == 'true'){
Event__c ev = [select id,Event_Setup_Start_Time__c,Event_Setup_End_Time__c from Event__c where id =: eventId ];

ev.Event_Setup_Start_Time__c = startDATE;
ev.Event_Setup_End_Time__c = endDATE;
ev.Start_Date__c = startDATE;
ev.end_DATE__c   = endDATE;
update ev; 
}**/
        } catch(Exception e) {
            system.debug(e.getTypeName() + ' Exception : ' + e.getMessage() + ' - at line number: ' + e.getLineNumber());
        }
    }
    
    public set<string> get_Rooms(Datetime startTimeSlot, Datetime endTimeSlot){
        set<string> roomID = new set<string>(); 
        roomID.clear();
        rmIdSEt.clear();
        system.debug('11-24-17 startTimeSlot = ' + startTimeSlot);
        system.debug('11-24-17 endTimeSlot = ' + endTimeSlot);
        DateTime dT = startTimeSlot;
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        // ADDED BY Kumar Sarkar, 11-24-17 [START]
        Date myEndDate = date.newinstance(endTimeSlot.year(), endTimeSlot.month(), endTimeSlot.day());
        long myEndTime = endTimeSlot.getTime();
        // ADDED BY Kumar Sarkar, 11-24-17 [END]
        long startTime = startTimeSlot.getTime();
        list<Room_Request__c> reqList = [SELECT id,Name, Room__c,Start_DateTime__c,End_DateTime__c FROM Room_Request__c where Status__c IN ( 'Approved') AND End_DateTime__c <> null ORDER BY Name DESC];
        
        for(Room_Request__c rq : reqList){
            dateTime d = rq.End_DateTime__c;
            Date myDate1 = date.newinstance(d.year(), d.month(), d.day());
            long rqStartTime = rq.Start_DateTime__c.getTime();
            long endTime = rq.End_DateTime__c.getTime();
            if((myDate == myDate1 || myEndDate == myDate1 ||myDate < myDate1 ) && endTime >startTime){
                roomID.add(rq.Room__c);
            }
        }

        return roomID;
    }
    
    // Executed when filter dropdown (1) on event types is changed
    public void fieldDropdownChangeHandler()
    {
        SubFieldList = new list<SelectOption>();
        SubFieldList.add(new SelectOption('-','-'));
        
        if(selectedField.toUpperCase().contains('TYPE'))
        {
            for(string typefiled : typeList)
                SubFieldList.add(new SelectOption(typefiled, typefiled));
        }
        else if(selectedField.toUpperCase().contains('BUILDING'))
        {
            for(string buildingFiled : buildingList)
                SubFieldList.add(new SelectOption(buildingFiled, buildingFiled));
        }
        else
        {
            SubFieldList = null;
        }
        system.debug('selectedField = ' + selectedField);
        system.debug('SubFieldList = ' + SubFieldList);
    }

    // Executed when second dependent dropdown, event name, is changed
    public void FildNameChangeHandler()
    {
       callevent();
    }

    // method to design the wrapper class list
    public void get_Rooms1(set<string> allRoomsAvailable)
    {
        string query = 'SELECT id,Name, Room_Number__c, Building__r.Name, Capacity__c, Type__c FROM Room__c ';
        query += 'WHERE Not_Available__c = false ';

        if(SelectedCategoiesField <> null){
            if(selectedField.toUpperCase().contains('TYPE'))
            {
                query += ' AND  Type__c != NULL AND Type__c = \'' + SelectedCategoiesField + '\'';
            }else   if(selectedField.toUpperCase().contains('BUILDING'))
            {
                query += 'AND  Building__r.Name != NULL AND Building__r.Name = \'' + SelectedCategoiesField + '\'';
            }
        }
        
        if(openedInLightningComponent){
            query += 'AND isAvailableForStudents__c = TRUE ';
        }

        List<Room__c> rmList = Database.query(query);
        listwrapper = new List<wrapper>();
        listwrapper1 = new List<wrapper>();
        listwrapper2 = new List<wrapper>();
        
        for(Room__c room : rmList) {
            if(room.Name == 'Training Room 1'){
                system.debug(room.Id);
                system.debug(allRoomsAvailable.contains(room.id));
            }
            if(!(allRoomsAvailable.size() <> 0  && allRoomsAvailable.contains(room.id))){
                listwrapper1.add(new wrapper(room.Id, room.Name, room.Room_Number__c, room.Building__r.Name, Integer.valueOf(room.Capacity__c), room.Type__c, false, true,eventId));
            }
            else {
                listwrapper2.add(new wrapper(room.Id, room.Name, room.Room_Number__c, room.Building__r.Name, Integer.valueOf(room.Capacity__c), room.Type__c, false, false,eventId));
            }
        }

        if(listwrapper1.size() <> 0)
            listwrapper.addAll(listwrapper1) ; 
        if(listwrapper2.size() <> 0)
            listwrapper.addAll(listwrapper2) ;

        mapWrapper = new map<integer,wrapper>();
        
        for(Integer index = 0 ; index < listwrapper.size() ; index++) {
            mapWrapper.put(index, listwrapper[index]);
        }
    }

    // class to wrap UI-requied fields of the rooms with the selection checkboxes.
    public  class wrapper{
        
        public String Id{get;set;}
        public String Name{get;set;}
        public String roomNumber{get;set;}
        public String building{get;set;}
        public Integer capacity{get;set;}
        public String type{get;set;}
        public Boolean isSelected {get;set;}
        public Boolean isAvailable {get;set;}
        public string eIDs {get;set;} 
        
        public wrapper(String Id, String Name, String roomNumber, String building, Integer capacity, String type, Boolean isSelected, Boolean isAvailable,string eIDs) {
            this.Id = Id;
            this.Name = Name;
            this.roomNumber = roomNumber;
            this.building = building;
            this.capacity = capacity;
            this.type = type;
            this.isSelected = isSelected;
            this.isAvailable = isAvailable;
            this.eIDs         = eIds;
        }
    }
}