@isTest (seeAllData = false)
public class EventRegistrationTest {
    
    public static Event__c primaryEvent;
    public static Event__c subEvent1;
    public static Event__c subEvent2;
    public static Discount_Code__c  discountCode;
    
    
    public static void startTests() {
        buildData();
        
        Test.setCurrentPageReference(new PageReference('Page.EventRegistration'));
        ApexPages.currentPage().getParameters().put('id' , primaryEvent.Id);
        
        //list<pymt__Processor_Connection__c> connection = [SELECT Id FROM pymt__Processor_Connection__c LIMIT 1];
        //if (connection.size() > 0) ApexPages.currentPage().getParameters().put('connectionid' , connection[0].Id);
        pymt__Settings__c settings = new pymt__Settings__c();
        insert settings;
        pymt__Processor_Connection__c connection = new pymt__Processor_Connection__c(pymt__PaymentConnect_Setup__c = settings.Id);
        insert connection;
        ApexPages.currentPage().getParameters().put('connectionid' , connection.Id);
        EventListController ev = new EventListController();
        newEventListController ev1 = new newEventListController();
        ev1.getMyRegistrations();
     ev.getMyRegistrations();
        ev.get_MyRegistrations();
       
    }
    
    public static testMethod void validateRegistration_ExistingContact_Paid() {
        Test.startTest();
        startTests();
        Contact con = buildContact();
        
        EventRegistration er = new EventRegistration();
        //ADDED BY Kumar Sarkar, 12-05-17 [START]
        list<SelectOption> tempList = er.getPaymentChoices();
        //ADDED BY Kumar Sarkar, 12-05-17 [END]
        // select events for registration
        ApexPages.currentPage().getParameters().put('eventId', subEvent1.Id);
        er.pageAction();
        er.toggleEventSelect();
        ApexPages.currentPage().getParameters().put('eventId', subEvent2.Id);
        er.pageAction();
        er.toggleEventSelect();
        
        // set donation amount
        er.donationAmt = 100;
        
        // show the contact form
        er.toggleShowContactForm();
        er.clearDonation();
        // add and remove guests
        er.addRegistrant();
        er.addRegistrant();
        er.toggleShowEventsListing();
        er.toggleShowDonationForm();
        er.toggleShowFirstForm();
        er.closePopup();
        er.redirectPopup();
        er.checkwaitlist();
        ApexPages.currentPage().getParameters().put('registrantNumber', '2');
        er.removeRegistrant();
        
        // fill in guest first/last names
        for (integer i = 0; i < er.registration.size(); i++) {
            er.registration[i].con.FirstName = 'fname';
            er.registration[i].con.LastName = 'lname';
        }
        
        // fill in contact information
        er.primaryReg.con.FirstName = 'TestFirst';
        er.primaryReg.con.LastName = 'TestLast';
        er.primaryReg.con.Email = 'test@test.com';
        er.primaryReg.con.Phone = '2223334455';
        er.primaryReg.con.MailingStreet = '123 Test Dr.';
        er.primaryReg.con.MailingCity = 'TestCity';
        er.primaryReg.con.MailingState = 'Texas';
        er.primaryReg.con.MailingPostalCode = '77382';
        //er.sendRegistrationEmail('testuser@gmail.com',con);
        er.doRegister();
        
        Test.stopTest();
    }
    
    public static testMethod void validateRegistration_NoContact_Free() {
        Test.startTest();
        startTests();
        
        EventRegistration er = new EventRegistration();
        // select events for registration
        ApexPages.currentPage().getParameters().put('eventId', subEvent1.Id);
        //ADDED BY Kumar Sarkar, 12-05-17
        er.pageAction();
        er.toggleEventSelect();
        
        // show the contact form
        er.toggleShowContactForm();
        
        // add a guest
        er.addRegistrant();
        ApexPages.currentPage().getParameters().put('registrantNumber', '2');
        
        // fill in guest first/last names
        for (integer i = 0; i < er.registration.size(); i++) {
            er.registration[i].con.FirstName = 'fname';
            er.registration[i].con.LastName = 'lname';
        }
        
        // fill in contact information
        er.primaryReg.con.FirstName = 'TestFirst';
        
        er.primaryReg.con.LastName = 'TestLast';
        er.primaryReg.con.Email = 'test@test.com';
        er.primaryReg.con.Phone = '2223334455';
        er.primaryReg.con.MailingStreet = '123 Test Dr.';
        er.primaryReg.con.MailingCity = 'TestCity';
        er.primaryReg.con.MailingState = 'Texas';
        er.primaryReg.con.MailingPostalCode = '77382';
        er.primaryReg.con.MailingCountry = 'United States';
        er.doRegister();
        
        Test.stopTest();
    }
    public static testMethod void validateRegistration_ExistingContact_Paid12() {
        Test.startTest();
        startTests();
        Event__c   primaryEvent11 = new Event__c();
        primaryEvent11.Name = 'Primary Event';
         primaryEvent11.Max_Registration__c=10;
        primaryEvent11.Registration_Fee__c = 0;
        primaryEvent11.Public__c = true;
        primaryEvent11.Start_Date__c = system.now().addMinutes(40);
        primaryEvent11.End_Date__c = system.now().addMinutes(60);
        primaryEvent11.Event_Location__c = 'Off-Campus';
        insert primaryEvent11;
        
        Event__c subEvent11 = new Event__c();
        subEvent11.Parent_Event__c = primaryEvent11.Id;
        subEvent11.Name = 'Sub Event 1';
         subEvent11.Max_Registration__c=10;
        subEvent11.Registration_Fee__c = 0;
        subEvent11.Public__c = true;
        subEvent11.Start_Date__c = system.now().addMinutes(40);
        subEvent11.End_Date__c = system.now().addMinutes(60);
        subEvent11.Event_Location__c = 'Off-Campus';
        insert subEvent11;
        
        Event__c subEvent21 = new Event__c();
        subEvent21.Parent_Event__c = primaryEvent11.Id;
        subEvent21.Name = 'Sub Event 2';
         subEvent21.Max_Registration__c=10;
        subEvent21.Registration_Fee__c = 100;
        subEvent21.Public__c = true;
        subEvent21.Start_Date__c = system.now().addMinutes(40);
        subEvent21.End_Date__c = system.now().addMinutes(60);
        insert subEvent21;
        
        discountCode = new Discount_Code__c();
        discountCode.name = 'discount code1';
        discountCode.Discount_Code__c = primaryEvent.Id;
        discountCode.Status__c = 'Active';
        discountCode.Discount_Value__c=10;
        insert discountCode;
        
        Building__c b =New Building__c(Name = 'tower c');
        insert b;
        Room__c r = New Room__c(Name='testing',Room_Number__c = '121',Building__c = b.Id);
        insert r;
        Room_Request__c roomRequest = New Room_Request__c(Room__c = r.Id,Event__c = primaryEvent11.Id,isPrimary__c = true);
        insert roomRequest ;
        Room_Request__c roomRequestForChildEvent = New Room_Request__c(Room__c = r.Id,Event__c = subEvent11.Id,isPrimary__c = true);
        insert roomRequestForChildEvent ;
        //
        ApexPages.currentPage().getParameters().put('eventId', subEvent11.Id);
        ApexPages.currentPage().getParameters().put('eventId', subEvent21.Id);
        ApexPages.currentPage().getParameters().put('DiscountId', discountCode.Id);
        EventRegistration er = new EventRegistration();
        //ADDED BY Kumar Sarkar, 12-05-17
        er.pageAction();
        testPageForTestingInCommunityController rr=new testPageForTestingInCommunityController();
        testPageForTestingInCommunityController.get_EventsList();
        er.DiscountName='discount code1';
        // set donation amount
        er.donationAmt = 100;
        
        // show the contact form
        er.toggleShowContactForm();
        er.clearDonation();
        er.AvailableDiscounts();
        // add and remove guests
        er.addRegistrant();
        er.addRegistrant();
        er.toggleShowEventsListing();
        er.toggleShowDonationForm();
        er.toggleShowFirstForm();
        er.closePopup();
        er.redirectPopup();
        er.checkwaitlist();
        ApexPages.currentPage().getParameters().put('registrantNumber', '2');
        er.removeRegistrant();
       
        // fill in guest first/last names
        for (integer i = 0; i < er.registration.size(); i++) {
            er.registration[i].con.FirstName = 'fname';
            er.registration[i].con.LastName = 'lname';
        }
        
        // fill in contact information
        er.primaryReg.con.FirstName = 'TestFirst';
        er.primaryReg.con.LastName = 'TestLast';
        er.primaryReg.con.Email = 'test@test.com';
        er.primaryReg.con.Phone = '2223334455';
        er.primaryReg.con.MailingStreet = '123 Test Dr.';
        er.primaryReg.con.MailingCity = 'TestCity';
        er.primaryReg.con.MailingState = 'Texas';
        er.primaryReg.con.MailingPostalCode = '77382';
        //er.sendRegistrationEmail('testuser@gmail.com',con);
        er.doRegister();
        
        Test.stopTest();
    }
    
    public static testMethod void validateErrorMessages() {
        Test.startTest();
        startTests();
        EventRegistration er = new EventRegistration();
        //ADDED BY Kumar Sarkar, 12-05-17
        er.pageAction();
        // show the contact form
        er.toggleShowContactForm();
        //EDITED BY Kumar Sarkar, 12-05-17
        //er.doRegister();
        
        Test.stopTest();
    }
    
    public static testMethod void validateRegistration_Misc() {
        Test.startTest();
        startTests();
        
        // pardot logic
        Contact con = buildContact();
        ApexPages.currentPage().getParameters().put('contactid' , con.Id);
        
        // instatiate controller
        EventRegistration er = new EventRegistration();
        //ADDED BY Kumar Sarkar, 12-05-17 [START]
        er.pageAction();
        er.addRegistrant();
        er.addRegistrant();
        //ADDED BY Kumar Sarkar, 12-05-17 [END]
        //er.payAtEvent = true;
        
        Test.stopTest();
    }
    
    
    public static void buildData() {
        primaryEvent = new Event__c();
        primaryEvent.Name = 'Primary Event';
        primaryEvent.Max_Registration__c=10;
        primaryEvent.Registration_Fee__c = 0;
        primaryEvent.Public__c = true;
        primaryEvent.Start_Date__c = system.now().addDays(2);
        primaryEvent.End_Date__c = system.now().addDays(2);
        primaryEvent.Event_Location__c = 'On-Campus';
        insert primaryEvent;
        
        subEvent1 = new Event__c();
        subEvent1.Parent_Event__c = primaryEvent.Id;
        subEvent1.Max_Registration__c=10;
        subEvent1.Name = 'Sub Event 1';
        subEvent1.Registration_Fee__c = 0;
        subEvent1.Public__c = true;
        subEvent1.Start_Date__c = system.now().adddays(2);
        subEvent1.End_Date__c = system.now().addDays(2);
        subEvent1.Event_Location__c = 'On-Campus';
        insert subEvent1;
        
        subEvent2 = new Event__c();
        subEvent2.Parent_Event__c = primaryEvent.Id;
        subEvent2.Max_Registration__c=10;
        subEvent2.Name = 'Sub Event 2';
        subEvent2.Registration_Fee__c = 100;
        subEvent2.Public__c = true;
        subEvent2.Start_Date__c = system.now().addMinutes(40);
        subEvent2.End_Date__c = system.now().addMinutes(60);
        insert subEvent2;
        
        discountCode = new Discount_Code__c();
        discountCode.name = 'discount code1';
        discountCode.Discount_Code__c = primaryEvent.Id;
        discountCode.Status__c = 'Active';
        insert discountCode;
        
        Building__c b =New Building__c(Name = 'tower c');
        insert b;
        Room__c r = New Room__c(Name='testing',Room_Number__c = '121',Building__c = b.Id);
        insert r;
        Room_Request__c roomRequest = New Room_Request__c(Room__c = r.Id,Event__c = primaryEvent.Id,isPrimary__c = true);
        insert roomRequest ;
        Room_Request__c roomRequestForChildEvent = New Room_Request__c(Room__c = r.Id,Event__c = subEvent1.Id,isPrimary__c = true);
        insert roomRequestForChildEvent ;
        
        
    }
    
    public static Contact buildContact() {
        Contact con = new Contact();
        con.FirstName = 'Fname';
        con.LastName = 'Lname';
        con.Email = 'test@test.com';
        con.Phone = '4455567788';
        
        insert con;
        return con;
    }
}