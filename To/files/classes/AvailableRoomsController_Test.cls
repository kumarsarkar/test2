@isTest 
public class AvailableRoomsController_Test {
    
    static testMethod void WithoutRec() 
	 {
         Building__c buil = new Building__c(name = 'TowerA');
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25,Type__c = 'Lab',Building__C=buil.id, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id, Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(40), Event_Setup_End_Time__c=DateTime.now().addMinutes(60),
                                         Start_Date__c=DateTime.now().addMinutes(40),
		End_Date__c=DateTime.now().addMinutes(60));
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('roomId', String.valueOf(testRoom.Id));
            pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
		    

			Test.setCurrentPage(pageRef);

			AvailableRoomsController Avail = new AvailableRoomsController();
         String Id          = testRoom.id;
         String Name        = testRoom.Name;
         String roomNumber  = testRoom.Room_Number__c;
         String building    = testRoom.Building__r.Name;
         Integer capacity   = Integer.valueOf(testRoom.Capacity__c);
         String type        = testRoom.Type__c;
         Boolean isSelected = false;
         Boolean isAvailable= true;
         string eids        = testEvent.id;
         
       AvailableRoomsController.wrapper listwrapper = new AvailableRoomsController.wrapper(Id,Name,roomNumber,building,capacity,type,isSelected,isAvailable,eids);
        
       
         //wrp.name = 'Test Wrapper';
          Avail.eventStartDate      = system.today();
          Avail.eventEndDate        = system.today();
         Avail.selectedField                = 'Type';
         Avail.SelectedCategoiesField       = 'Lab';
         Avail.eventStartTime               = '12:00 PM';
         Avail.eventEndTime                 = '12:30 PM';
        Avail.callevent();
         Avail.fieldDropdownChangeHandler();
         Avail.saveRoomRequests();
         
         
			//testAccPlan.pageLoad();
     		
		Test.StopTest();
	 }
    
     static testMethod void WithRec() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420',Type__c='Lab', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Recurring__c=true, Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(40), Event_Setup_End_Time__c=DateTime.now().addMinutes(60),
                                         Start_Date__c=DateTime.now().addMinutes(40),
		End_Date__c=DateTime.now().addMinutes(60));
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('roomId', String.valueOf(testRoom.Id));
            pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
		

			Test.setCurrentPage(pageRef);

			AvailableRoomsController Avail = new AvailableRoomsController();
			//testAccPlan.pageLoad();
     		  Avail.selectedField                = 'Building';
              Avail.SelectedCategoiesField       = testRoom.Type__c;
          Avail.fieldDropdownChangeHandler();
         Avail.FildNameChangeHandler();
		Test.StopTest();
	 }
    
    static testMethod void WithRecWeekly() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
       Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Weekly__c=true,Monday__c = true,Type__c='Alumni', Category__c='New Student', Tuesday__c=true,Wednesday__c=true,Thursday__c=true,Friday__c=true,Saturday__c=true,Sunday__c=true,Event_Location__c='On-Campus', Registration_Fee__c=100,
       Event_Setup_Start_Time__c=DateTime.now().addMinutes(40), Event_Setup_End_Time__c=DateTime.now().addMonths(1).addMinutes(60),Start_Date__c=DateTime.now().addMinutes(40),
		End_Date__c=DateTime.now().addMinutes(60));
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('roomId', String.valueOf(testRoom.Id));
            pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
		

			Test.setCurrentPage(pageRef);

			AvailableRoomsController Avail = new AvailableRoomsController();
			//testAccPlan.pageLoad();
     		
		Test.StopTest();
	 }
    static testMethod void WithRecMonthly() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
        Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Monthly__c=true,Day_of__c='2nd',Day__c='Monday', Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100, Event_Setup_Start_Time__c=DateTime.now().addMinutes(40), Event_Setup_End_Time__c=DateTime.now().addMonths(2).addMinutes(60),Start_Date__c=DateTime.now().addMinutes(40),
		End_Date__c=DateTime.now().addMinutes(60));
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('roomId', String.valueOf(testRoom.Id));
            pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
		

			Test.setCurrentPage(pageRef);

			AvailableRoomsController Avail = new AvailableRoomsController();
			//testAccPlan.pageLoad();
     		
		Test.StopTest();
	 } 
    
    static testMethod void WithRecYearly() 
	 {
         School_College__c sch = new School_College__c(name = 'testSchool');
         insert sch;
         Department__c dprt = new Department__c(Name = 'TestDepart');
         insert dprt;
         Program__c   prog = new Program__c(Name = 'testProg');
         insert prog;
		 Room__c testRoom = new Room__c(Name='Test Room', Capacity__c=25, ColleagueUniqueID__c='420', Description__c='Test Room');
        insert testRoom;
      Event__c testEvent = new Event__c(Name='Test Event',Max_Registration__c=10,School_College__c=sch.id,Department__c=dprt.id,Program__c=prog.id,Recurring__c= true,Yearly__c=true,Day_of1__c='2nd',Every_Year__c='January',Day1__c='Sunday', Category__c='New Student', Event_Location__c='On-Campus', Registration_Fee__c=100,
       Event_Setup_Start_Time__c=DateTime.now().addMinutes(40), Event_Setup_End_Time__c=DateTime.now().addYears(12).addMinutes(60),Start_Date__c=DateTime.now().addMinutes(40),
		End_Date__c=DateTime.now().addMinutes(60));
        insert testEvent;
        Room_Request__c testRoomRequest = new Room_Request__c( Room__c=testRoom.Id, Event__c=testEvent.Id, Status__c='Pending Approval', Request_Type__c='Reservation', Start_DateTime__c=DateTime.now().addMinutes(-120), End_DateTime__c=DateTime.now().addMinutes(120));
        insert testRoomRequest;
        Room_Booking__c testRoomBooking = new Room_Booking__c(Name='Test Room Booking', Room__c=testRoom.Id, Room_Request__c=testRoomRequest.Id, Start_Timeslot__c=testRoomRequest.Start_DateTime__c, End_Timeslot__c=testRoomRequest.End_DateTime__c);
        insert testRoomBooking;
	
		Test.StartTest(); 

			PageReference pageRef = Page.EventsCalendar; // Add your VF page Name here
			pageRef.getParameters().put('roomId', String.valueOf(testRoom.Id));
            pageRef.getParameters().put('eventId', String.valueOf(testEvent.Id));
		

			Test.setCurrentPage(pageRef);

			AvailableRoomsController Avail = new AvailableRoomsController();
			//testAccPlan.pageLoad();
     		
		Test.StopTest();
	 } 
}