/*********************************************************************************
* Name: RoomRequestTriggerHandler 
* Created by   {DateCreated 9/21/2017} 
----------------------------------------------------------------------------------
* Purpose/Methods:/ 
* This  is used as handler class for trigger RoomRequestTrigger
----------------------------------------------------------------------------------

* History:
* VERSION DEVELOPER NAME DATE DETAIL FEATURES
1.0 9/21/2017 INITIAL DEVELOPMENT
 
*********************************************************************************/
public class RoomRequestTriggerHandler {
  
    public static void ConfirmRoomAvailability(List<Room_Request__c> newList) {
        
        List<Room_Request__c> lstAvlbleRoomReq = new List<Room_Request__c>();
        
        List<Room_Booking__c> lstRoomBooking = new List<Room_Booking__c>();
        
        lstAvlbleRoomReq = [SELECT id,Name,End_DateTime__c,Event__c,Room__c,Start_DateTime__c,Status__c from Room_Request__c WHERE id IN: newList AND Status__c = 'Pending Approval'];
        
        if(!lstAvlbleRoomReq.IsEmpty()){
            for(Room_Request__c objRoomReq: lstAvlbleRoomReq)
            {
                lstRoomBooking = [Select Id, Name,Room_Request__c from Room_Booking__c where Room_Request__c =: objRoomReq.Id LIMIT 1];
                IF(lstRoomBooking.IsEmpty()){
                    Room_Booking__c recRoomBooking = new Room_Booking__c(
                    Name=objRoomReq.Name,
                    Room_Request__c=objRoomReq.Id,
                    Room__c=objRoomReq.Room__c,
                    Current_Reservation_State__c='Pending Approval',
                    Start_Timeslot__c=objRoomReq.Start_DateTime__c,
                    End_Timeslot__c=objRoomReq.End_DateTime__c
                    );
                    insert recRoomBooking;
                
                }
            }
          
        }
    }
    
    public static void CancelRoomAvailability(List<Room_Request__c> newList) {
        
        List<Room_Request__c> lstAvlbleRoomReq = new List<Room_Request__c>();
        
        List<Room_Booking__c> lstRoomBooking = new List<Room_Booking__c>();
        
        lstAvlbleRoomReq = [SELECT id,Name,End_DateTime__c,Event__c,Room__c,Start_DateTime__c,Status__c from Room_Request__c WHERE id IN: newList AND Status__c = 'Cancelled'];
        
        if(!lstAvlbleRoomReq.IsEmpty()){
            for(Room_Request__c objRoomReq: lstAvlbleRoomReq)
            {
                lstRoomBooking = [Select Id, Name,Room_Request__c from Room_Booking__c where Room_Request__c =: objRoomReq.Id LIMIT 1];
                IF(!lstRoomBooking.IsEmpty()){
                    
                    lstRoomBooking[0].Current_Reservation_State__c='Cancelled';
                    update lstRoomBooking;
                }
            }
          
        }
    }
    public static void UpdateRoomAvailability(List<Room_Request__c> newList) {
        
        List<Room_Request__c> lstAvlbleRoomReq = new List<Room_Request__c>();
        
        List<Room_Booking__c> lstRoomBooking = new List<Room_Booking__c>();
        
        lstAvlbleRoomReq = [SELECT id,Name,End_DateTime__c,Event__c,Room__c,Start_DateTime__c,Status__c from Room_Request__c WHERE id IN: newList AND Status__c = 'Approved'];
        
        if(!lstAvlbleRoomReq.IsEmpty()){
            for(Room_Request__c objRoomReq: lstAvlbleRoomReq)
            {
                lstRoomBooking = [Select Id, Name,Room_Request__c from Room_Booking__c where Room_Request__c =: objRoomReq.Id LIMIT 1];
                IF(!lstRoomBooking.IsEmpty()){
                    
                    lstRoomBooking[0].Current_Reservation_State__c='Reserved';
                    update lstRoomBooking;
                
                }
            }
          
        }
    }
    public static void RejectRoomAvailability(List<Room_Request__c> newList,List<Room_Request__c> oldList) {
        
        List<Room_Request__c> lstAvlbleRoomReq = new List<Room_Request__c>();
        
        List<Room_Booking__c> lstRoomBooking = new List<Room_Booking__c>();
        
        lstAvlbleRoomReq = [SELECT id,Name,End_DateTime__c,Event__c,Room__c,Start_DateTime__c,Status__c from Room_Request__c WHERE id IN: newList AND Status__c = 'Rejected'];
        
        if(!lstAvlbleRoomReq.IsEmpty()){
            for(Room_Request__c objRoomReq: lstAvlbleRoomReq)
            {
                lstRoomBooking = [Select Id, Name,Room_Request__c from Room_Booking__c where Room_Request__c =: objRoomReq.Id LIMIT 1];
                IF(!lstRoomBooking.IsEmpty()){
                    
                    lstRoomBooking[0].Current_Reservation_State__c='Rejected';
                    update lstRoomBooking;
                
                }
            }
          
        }
    }
}