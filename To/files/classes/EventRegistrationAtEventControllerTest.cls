@isTest
public Class EventRegistrationAtEventControllerTest{

    public static testMethod void testEventConfirmation(){
        
    string firstName = 'firstname';
    string lastname = 'lastname';
    string mailId = 'sjoshi@huronconsultinggroup.com';
    string eventName = 'event name';
    
    Event__c e = new Event__c(name = 'event name',Max_Registration__c=10,Event_Location__c = 'On-Campus',Start_Date__c = system.today().addDays(1),End_Date__c = system.today().addDays(2));
    insert e;
    Building__c b =New Building__c(Name = 'tower c');
        insert b;
        Room__c r = New Room__c(Name='testing',Room_Number__c = '121',Building__c = b.Id);
        insert r;
        Room_Request__c roomRequest = New Room_Request__c(Room__c = r.Id,Event__c = e.Id,isPrimary__c = true);
        insert roomRequest ;
    
    EventRegistrationAtEventController acc= new EventRegistrationAtEventController(firstName,lastname,mailId,eventName);
    acc.validateInputData();
    
    } 
}