public class RoomAvailabilityUtil {
    public List<wrapper> listwrapper {get;set;}
    Set<id> set1 = new Set<Id>();
    Set<id> set2 = new Set<Id>();
    public RoomAvailabilityUtil()
    {
    }
    
    public List<Room__c> get_Rooms(Datetime startTimeSlot, Datetime endTimeSlot)
    {
        String startDate = (date.newinstance(startTimeSlot.year(), startTimeSlot.month(), startTimeSlot.day())).format().split(' ')[0];
        String endDate = (date.newinstance(endTimeSlot.year(), endTimeSlot.month(), endTimeSlot.day())).format().split(' ')[0];
        Map<Id, Room__c> allRoomsAvailableMap = new Map<Id, Room__c>();
        Map<Id, Room__c> filteredRoomsMap1;
        Map<Id, Room__c> filteredRoomsMap2;
        
        if(startDate == endDate)
        {
            filteredRoomsMap1 = new Map<Id, Room__c>([SELECT Name,
                                                        Room_Number__c,
                                                        Building__c,
                                                        Capacity__c,
                                                    Type__c FROM Room__c
                                                    WHERE Id NOT IN (SELECT Room__c FROM Room_Booking__c
                                                                        WHERE Current_Reservation_State__c IN ('Pending Approval', 'Reserved')
                                                                            AND Start_Timeslot__c != NULL
                                                                            AND ((Start_Timeslot__c >=: startTimeSlot
                                                                                AND Start_Timeslot__c <=: endTimeSlot) 
                                                                            OR (End_Timeslot__c >=: startTimeSlot
                                                                                AND End_Timeslot__c <=: endTimeSlot)))
                                                                        ORDER BY Name]);
            
            filteredRoomsMap2 = new Map<Id, Room__c>([SELECT Name,
                                                        Room_Number__c,
                                                        Building__c,
                                                        Capacity__c,
                                                    Type__c FROM Room__c
                                                    WHERE Id NOT IN (SELECT Room__c FROM Room_Request__c
                                                                        WHERE Status__c IN ('Pending Approval', 'Approved')
                                                                            AND Start_DateTime__c != NULL
                                                                            AND ((Start_DateTime__c >=: startTimeSlot
                                                                                AND Start_DateTime__c <=: endTimeSlot) 
                                                                            OR (End_DateTime__c >=: startTimeSlot
                                                                                AND End_DateTime__c <=: endTimeSlot)))
                                                                        ORDER BY Name]);
            set1 = filteredRoomsMap2.keySet();
        }
        else
        {
            Time startTime = startTimeSlot.timeGMT();
            Time endTime = endTimeSlot.timeGMT();
            system.debug('startTimeSlot = ' + startTimeSlot);
            system.debug('endTimeSlot = ' + endTimeSlot);
            system.debug('startTime = ' + startTime);
            system.debug('endTime = ' + endTime);

            Map<Id, Room_Booking__c> tempRB = new Map<Id, Room_Booking__c>([SELECT Room__c, Start_Timeslot__c, End_Timeslot__c FROM Room_Booking__c
                                                WHERE Current_Reservation_State__c IN ('Pending Approval', 'Reserved')
                                                    AND Start_Timeslot__c != NULL
                                                    AND ((Start_Timeslot__c >=: startTimeSlot
                                                        AND Start_Timeslot__c <=: endTimeSlot) 
                                                    OR (End_Timeslot__c >=: startTimeSlot
                                                        AND End_Timeslot__c <=: endTimeSlot))
                                                ORDER BY Room__c, End_Timeslot__c]);
            Map<Id, Room_Request__c> tempRR = new Map<Id, Room_Request__c>([SELECT Room__c, Start_DateTime__c, End_DateTime__c FROM Room_Request__c
                                                WHERE Status__c IN ('Pending Approval', 'Approved')
                                                    AND Start_DateTime__c != NULL
                                                    AND ((Start_DateTime__c >=: startTimeSlot
                                                        AND Start_DateTime__c <=: endTimeSlot) 
                                                    OR (End_DateTime__c >=: startTimeSlot
                                                        AND End_DateTime__c <=: endTimeSlot))
                                                ORDER BY Room__c, End_DateTime__c]);
            
            Set<Id> roomReqIds = new Set<Id>();
            for(Room_Request__c tempRRVar : tempRR.values())
                if((tempRRVar.Start_DateTime__c.timeGMT() <= startTime && startTime <= tempRRVar.End_DateTime__c.timeGMT()) || (tempRRVar.Start_DateTime__c.time() <= endTime && endTime <= tempRRVar.End_DateTime__c.time()))
                    roomReqIds.add(tempRRVar.Room__c);
            for(Room_Booking__c tempRBVar : tempRB.values())
                if((tempRBVar.Start_Timeslot__c.timeGMT() <= startTime && startTime <= tempRBVar.End_Timeslot__c.timeGMT()) || (tempRBVar.Start_Timeslot__c.time() <= endTime && endTime <= tempRBVar.End_Timeslot__c.time()))
                    roomReqIds.add(tempRBVar.Room__c);
            
            filteredRoomsMap2 = new Map<Id, Room__c>([SELECT Name,
                                                        Room_Number__c,
                                                        Building__c,
                                                        Capacity__c,
                                                    Type__c FROM Room__c
                                                    WHERE Id NOT IN: roomReqIds ORDER BY Name]);
            set2 = filteredRoomsMap2.keySet();
        }

        if(set1.size() > 0 && set2.size() > 0)
        {
            for(Id set2Var : set2)
                set1.remove(set2Var);
            system.debug(set1);
        }

        //allRoomsAvailableMap.putAll(filteredRoomsMap1);
        allRoomsAvailableMap.putAll(filteredRoomsMap2);
        get_Rooms(allRoomsAvailableMap.values());
        
        return allRoomsAvailableMap.values();
    }
    
    /*public Map<Id, Room_Booking__c> get_Timeslots(Room__c selectedRoom)
    {
        system.debug('selectedRoom = ' + selectedRoom);
        
        return null;
    }
    
    public Boolean get_RoomAvailability(Room__c selectedRoom, Datetime startTimeSlot, Datetime endTimeSlot)
    {
        system.debug('selectedRoom = ' + selectedRoom);
        system.debug('startTimeSlot = ' + startTimeSlot);
        system.debug('endTimeSlot = ' + endTimeSlot);
        
        return null;
    }*/
    
    // method to design the wrapper class list
    public void get_Rooms(List<Room__c> allRoomsAvailable)
    {
        listwrapper = new List<wrapper>();
        for(Room__c room : allRoomsAvailable) {
            listwrapper.add(new wrapper(room.Id, room.Name, room.Room_Number__c, room.Building__c, Integer.valueOf(room.Capacity__c), room.Type__c, false));
        }
        system.debug('listwrapper.size() = ' + listwrapper.size());
    }
    
    // class to wrap UI-requied fields of the rooms with the selection checkboxes.
    public class wrapper{
        
        public String Id{get;set;}
        public String Name{get;set;}
        public String roomNumber{get;set;}
        public String building{get;set;}
        public Integer capacity{get;set;}
        public String type{get;set;}
        public Boolean isSelected {get;set;}
        
        public wrapper(String Id, String Name, String roomNumber, String building, Integer capacity, String type, Boolean isSelected) {
            this.Id = Id;
            this.Name = Name;
            this.roomNumber = roomNumber;
            this.building = building;
            this.capacity = capacity;
            this.type = type;
            this.isSelected = isSelected;
        }
    }
}