@isTest
Public Class EventConfirmationAfterPaymentTriggerTest{

    public static testMethod void testEventConfirmation(){
        
        Contact con = New Contact(FirstName = 'first Name',lastname = 'lastname',Email='sjoshi@huronconsultinggroup.com');
        insert con;
        Event__c e = New Event__c(name = 'eventname',Max_Registration__c=10,Event_Location__c = 'On-Campus',Start_Date__c = System.now().addMinutes(60),End_Date__c =System.now().addMinutes(120),Event_Description__c = 'testing');
        insert e;
        Decimal amount = 100;
    
        pymt__PaymentX__c pymt = new pymt__PaymentX__c();
        pymt.Name = e.Name + ' Registration';
        pymt.pymt__Payment_Processor__c = Label.Payment_Processor;
        pymt.pymt__Processor_Connection__c = Label.Payment_Connection_ID; 
        pymt.pymt__Contact__c = con.Id;       
        pymt.pymt__Amount__c = amount;
        pymt.pymt__Transaction_Type__c = 'Payment';
        pymt.pymt__Status__c = 'In Process';
        pymt.pymt__Currency_ISO_Code__c = 'USD';
        pymt.pymt__Tax__c = 0;
        pymt.pymt__Shipping__c = 0;
        
        insert pymt;
        
        Event_Registration__c registrationRecord = New Event_Registration__c(Event__c = e.Id,First_Name__c='testing',Last_Name__c = 'lastname',Payment__c = pymt.Id,Contact__c=con.Id);
        insert registrationRecord ;
        Building__c b =New Building__c(Name = 'tower c');
        insert b;
        Room__c r = New Room__c(Name='testing',Room_Number__c = '121',Building__c = b.Id);
        insert r;
        Room_Request__c roomRequest = New Room_Request__c(Room__c = r.Id,Event__c = e.Id,isPrimary__c = true);
        insert roomRequest ;
        
        pymt.pymt__Status__c = 'Completed';
        update pymt;
    }

}